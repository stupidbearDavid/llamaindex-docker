"""Docs parser.

Contains parsers for docx, pdf files.

"""
import os
from lxml import etree
from pathlib import Path
from typing import Dict, List, Optional
import hashlib
# import fitz

from llama_index.readers.base import BaseReader
from llama_index.schema import Document

# root_path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + os.path.sep + ".") + '/'
# with open(root_path + '../../../config.conf', encoding='utf-8') as data:
#     i = 0
#     for line in data:
#         if i == 2:
#             server_location = line.strip()
#         i += 1
from config import server_location

namespaces = {
    'a': 'http://schemas.openxmlformats.org/drawingml/2006/main',
    'r': 'http://schemas.openxmlformats.org/officeDocument/2006/relationships',
    'wp': 'http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing',
    'pic': 'http://schemas.openxmlformats.org/drawingml/2006/picture'
}


def get_md5_hash(file_str):
    md5_hash = hashlib.md5(file_str.encode('utf-8')).hexdigest()
    return md5_hash


class PDFReader(BaseReader):
    """PDF parser."""

    def load_data(
        self, file: Path, extra_info: Optional[Dict] = None
    ) -> List[Document]:
        """Parse file."""
        try:
            import pypdf
        except ImportError:
            raise ImportError(
                "pypdf is required to read PDF files: `pip install pypdf`"
            )
        with open(file, "rb") as fp:
            # Create a PDF object
            pdf = pypdf.PdfReader(fp)

            # Get the number of pages in the PDF document
            num_pages = len(pdf.pages)

            # Iterate over every page
            docs = []
            for page in range(num_pages):
                # Extract the text from the page
                page_text = pdf.pages[page].extract_text()
                page_label = pdf.page_labels[page]

                metadata = {"page_label": page_label, "file_name": file.name}
                if extra_info is not None:
                    metadata.update(extra_info)

                docs.append(Document(text=page_text, metadata=metadata))
            return docs


# class PDFReader(BaseReader):
#     """PDF parser."""
#
#     def load_data(
#         self, file: Path, extra_info: Optional[Dict] = None
#     ) -> List[Document]:
#         """Parse file."""
#         try:
#             import pypdf
#
#         except ImportError:
#             raise ImportError(
#                 "pypdf is required to read PDF files: `pip install pypdf`"
#             )
#         filename = get_md5_hash(file.name)
#         image_path = os.path.join(os.path.join(root_path + '../../../service/uploads', filename))
#         url_path = server_location + "/cache/" + filename + '/'
#
#         if not os.path.exists(image_path):
#             os.mkdir(image_path)
#
#         docs = []
#         doc = fitz.open(file)
#         known_fontsize = 6
#
#         def adjust_text_box_and_fontsize(page, img_rect, text, desired_fontsize):
#             text_width = fitz.get_text_length(text, fontsize=desired_fontsize)
#
#             if text_width > img_rect.width:
#                 scale_factor = img_rect.width / text_width
#                 new_fontsize = desired_fontsize * scale_factor
#
#                 text_x = img_rect.x0
#                 text_y = img_rect.y1 + 10
#                 text_position = fitz.Point(text_x + 50, text_y)
#                 text_img = fitz.Point(text_x, text_y)
#                 page.insert_text(text_img, "如图所示", fontsize=new_fontsize, fontname="china-ss", color=(0, 0, 0))
#                 page.insert_text(text_position, text, fontsize=new_fontsize, color=(0, 0, 0))
#             else:
#                 text_img = fitz.Point(img_rect.x0, img_rect.y0)
#                 text_position = fitz.Point(img_rect.x0 + 50, img_rect.y0)
#                 page.insert_text(text_img, "如图所示", fontsize=desired_fontsize, fontname="china-ss", color=(0, 0, 0))
#                 page.insert_text(text_position, text, fontsize=desired_fontsize, color=(0, 0, 0))
#
#         xrefs = set()
#         for page_num, page in enumerate(doc):
#             for img_index, img in enumerate(page.get_images(full=True), start=1):
#                 xref = img[0]
#                 xrefs.add(xref)
#                 image_info = doc.extract_image(xref)
#                 image_data = image_info["image"]
#
#                 image_filename = f"{page_num + 1}_{img_index}.png"
#                 image = os.path.join(image_path, image_filename)
#
#                 with open(image, "wb") as image_file:
#                     image_file.write(image_data)
#
#                 image_rect = page.get_image_rects(img[0])
#                 img_rect = fitz.Rect(image_rect[0])
#
#                 page.add_redact_annot(img_rect)
#                 page.apply_redactions()
#
#                 text_to_insert = os.path.join(url_path, image_filename)
#                 adjust_text_box_and_fontsize(page, img_rect, text_to_insert, known_fontsize)
#
#             drawings = page.get_drawings()
#             for img_index, drawing in enumerate(drawings):
#                 # 只处理SVG类型的矢量图形
#                 if drawing['type'] == 'svg':
#                     # 获取SVG矢量图形的边界框
#                     rect = fitz.Rect(drawing['rect'])
#
#                     # 生成Bitmap
#                     pix = page.get_pixmap(clip=rect, matrix=fitz.Matrix(2.0, 2.0))
#
#                     # 保存Bitmap为图片
#                     img_filename = f'image_{page_index}_{i}.png'
#                     img_path = os.path.join(output_dir, img_filename)
#                     pix.save(img_path)
#
#             metadata = {"page_label": page_num, "file_name": file.name}
#             if extra_info is not None:
#                 metadata.update(extra_info)
#
#             docs.append(Document(text=page.get_text(), metadata=metadata))
#         return docs


class DocxReader(BaseReader):
    """Docx parser."""

    def load_data(
        self, file: Path, extra_info: Optional[Dict] = None
    ) -> List[Document]:
        """Parse file."""
        try:
            import docx2txt
        except ImportError:
            raise ImportError(
                "docx2txt is required to read Microsoft Word files: "
                "`pip install docx2txt`"
            )

        text = docx2txt.process(file)
        metadata = {"file_name": file.name}
        if extra_info is not None:
            metadata.update(extra_info)

        return [Document(text=text, metadata=metadata or {})]


# class DocxReader(BaseReader):
#     """Docx parser."""
#
#     def load_data(
#         self, file: Path, extra_info: Optional[Dict] = None
#     ) -> List[Document]:
#         """Parse file."""
#         try:
#             from docx import Document as Doc
#         except ImportError:
#             raise ImportError(
#                 "docx is required to read Microsoft Word files: "
#                 "`pip install docx`"
#             )
#
#         filename = get_md5_hash(file.name)
#         image_path = os.path.join(os.path.join(root_path + '../../../service/uploads', filename))
#         url_path = server_location + "/cache/" + filename + '/'
#
#         if not os.path.exists(image_path):
#             os.mkdir(image_path)
#
#         def process_images_from_docx(docx_file: Path):
#             doc = Doc(docx_file)
#             full_text = []
#
#             image_count = 0
#             for para in doc.paragraphs:
#                 para_text = ''  # 段落文本
#                 for run in para.runs:
#                     # 将run的r元素转化为XML Element对象
#                     r_element = etree.fromstring(run._r.xml)
#                     # 检查是否包含图形(drawings)
#                     picture_elements = r_element.findall('.//pic:pic', namespaces)
#                     if picture_elements:
#                         for picture in picture_elements:
#                             image_count += 1
#                             image_filename = f'{image_count}.png'  # 生成图片的文件名
#                             image_relID = picture.find('.//a:blip', namespaces).get(f'{{{namespaces["r"]}}}embed')
#
#                             # 根据relID从文档中提取图片
#                             image_part = doc.part.related_parts[image_relID]
#                             image_output = os.path.join(image_path, image_filename)
#
#                             # 将图片保存到文件
#                             with open(image_output, 'wb') as f:
#                                 f.write(image_part.blob)
#
#                             image = os.path.join(url_path, image_filename)
#
#                             # 添加Markdown形式的路径
#                             markdown_image_text = f"如图所示 {image}\n"
#                             para_text += markdown_image_text
#                     else:
#                         para_text += run.text
#
#                 full_text.append(para_text)
#
#             return '\n'.join(full_text)
#
#         text = process_images_from_docx(file)
#         metadata = {"file_name": file.name}
#         if extra_info is not None:
#             metadata.update(extra_info)
#
#         return [Document(text=text, metadata=metadata or {})]


class XlsxReader(BaseReader):
    """Docx parser."""

    def load_data(
        self, file: Path, extra_info: Optional[Dict] = None
    ) -> List[Document]:
        """Parse file."""
        try:
            import pandas as pd
        except ImportError:
            raise ImportError(
                "docx2txt is required to read Microsoft Word files: "
                "`pip install docx2txt`"
            )
        df = pd.read_excel(file)
        text = str(df.to_dict())
        metadata = {"file_name": file.name}
        if extra_info is not None:
            metadata.update(extra_info)

        return [Document(text=text, metadata=metadata or {})]
