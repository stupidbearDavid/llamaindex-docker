#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
import os
import tornado
import argparse

from service.webservice.deleteHander import DeleteHandler
from service.webservice.deleteIndexHander import DeleteIndexHandler
from service.webservice.filelist import FileListHandler
from service.webservice.index import Index
from service.webservice.fileHander import FileHandler
from service.webservice.indexListHander import IndexListHandler
from service.webservice.newIndexHander import NewIndexHandler
from service.webservice.viewHander import ViewHandler
from service.webservice.contentHander import ContentHandler
from service.webservice.authorityHander import AuthorityHandler

from service.webservice.queryHander import QueryHandler


def parse_arguments():
    parser = argparse.ArgumentParser(description='Tornado Web Server')
    parser.add_argument('--redis_url', type=str, default="localhost", required=False, help='URL to listen on')
    parser.add_argument('--email', type=str, default="stupidbeardavid@hotmail.com", required=False, help='mentalgpt registrated email')
    parser.add_argument('--port', type=int, default=6379, required=False, help='Port number to listen on')
    parser.add_argument('--password', type=str, default="5228363", required=False, help='Password to redis')
    return parser.parse_args()


root_path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + os.path.sep + ".") + '/'

args = parse_arguments()

with open(root_path + 'config.conf', 'w', encoding='utf-8') as data:
    data.write("redis://:" + args.password + "@" + args.redis_url + ":" + str(args.port) + '\n')
    data.write(args.email)


handlers = [
    (r'/upload', FileHandler),
    (r'/delete', DeleteHandler),
    (r'/query', QueryHandler),
    (r'/view', ViewHandler),
    (r'/content', ContentHandler),
    (r'/filelist', FileListHandler),
    (r'/newindex', NewIndexHandler),
    (r'/apilist', IndexListHandler),
    (r'/deleteindex', DeleteIndexHandler),
    (r'/authority', AuthorityHandler),
    (r'/', Index),
]

app = tornado.web.Application(handlers=handlers)
# http_server = tornado.httpserver.HTTPServer(app)
app.listen(8888)
tornado.ioloop.IOLoop.instance().start()
