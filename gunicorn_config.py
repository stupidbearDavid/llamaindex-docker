workers = 8
worker_class = 'gthread'
threads = 8

worker_connections = 1200

accesslogs = 'logs/gunicorn.log'
errorlogs = 'logs/error.log'

bind = '0.0.0.0:8888'