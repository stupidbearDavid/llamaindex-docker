import os
import argparse

root_path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + os.path.sep + ".") + '/'


def parse_arguments():
    # parser = argparse.ArgumentParser(description='Tornado Web Server')
    # parser.add_argument('--redis_url', type=str, required=True, help='URL to listen on')
    # parser.add_argument('--email', type=str, required=True, help='mentalgpt registrated email')
    # parser.add_argument('--redis_port', type=int, default=6379, required=False, help='Port number to listen on')
    # parser.add_argument('--redis_pwd', type=str, required=True, help='Password to redis')
    # parser.add_argument('--mysql_url', type=str, required=True, help='URL to listen on')
    # parser.add_argument('--mysql_user', type=str, required=True, help='Port number to listen on')
    # parser.add_argument('--mysql_pwd', type=str, required=True, help='Password to mysql')
    # parser.add_argument('--is_auth_check', type=bool, default=False, help='Is query auth check')
    # parser.add_argument('--server_location', type=str, default="", required=False, help='Password to redis')

    parser = argparse.ArgumentParser(description='Tornado Web Server')
    parser.add_argument('--redis_url', type=str, required=False, default="llamaindex.mentalgpt.cc",
                        help='URL to listen on')
    parser.add_argument('--email', type=str, required=False, default="sd185115@163.com,seallhf@qq.com",
                        help='mentalgpt registrated email')
    parser.add_argument('--redis_port', type=int, default=6379, required=False, help='Port number to listen on')
    parser.add_argument('--redis_pwd', type=str, required=False, default="5228363", help='Password to redis')
    parser.add_argument('--mysql_url', type=str, required=False, default="127.0.0.1", help='URL to listen on')
    parser.add_argument('--mysql_port', type=int, required=False, default=6306, help='port to listen on')
    parser.add_argument('--mysql_user', type=str, required=False, default="luhaofang", help='user name to listen on')
    parser.add_argument('--mysql_pwd', type=str, required=False, default="12345678", help='Password to mysql')
    parser.add_argument('--neo4j_url', type=str, required=False, default="127.0.0.1", help='neo4j URL to listen on')
    parser.add_argument('--neo4j_port', type=str, required=False, default="7687", help='neo4j URL to listen on')
    parser.add_argument('--neo4j_user', type=str, required=False, default="neo4j", help='neo4j username to access')
    parser.add_argument('--neo4j_pwd', type=str, required=False, default="neo4juser", help='Password to neo4j')
    parser.add_argument('--neo4j_db', type=str, required=False, default="neo4j", help='neo4j database')
    parser.add_argument('--is_auth_check', type=bool, default=False, help='Is query auth check')
    parser.add_argument('--is_serial_content', type=bool, default=False, help='Is detect serial content and synthesis')
    parser.add_argument('--is_rerank', type=bool, default=False, help='Is rerank the result')
    parser.add_argument('--db_type', type=str, default="redis", help='database type')
    parser.add_argument('--chunk_size', type=int, default=512, help='chunk size')
    parser.add_argument('--overlap_size', type=int, default=20, help='overlap size')
    parser.add_argument('--server_location', type=str, default="", required=False, help='Password to redis')
    parser.add_argument('--faq_location', type=str, default="https://testwhadmin.cicba.cn/api/api/knowledge/faq",
                        required=False, help='faq location')
    parser.add_argument('--item_location', type=str, default="https://testwhadmin.cicba.cn/api/product/getAll",
                        required=False, help='faq location')
    parser.add_argument('--relation_location', type=str,
                        default="https://testwhadmin.cicba.cn/api/api/knowledge/relation",
                        required=False, help='faq location')
    parser.add_argument('--character_location', type=str,
                        default="https://testwhadmin.cicba.cn/api/api/knowledge/getInternetId",
                        required=False, help='faq location')
    parser.add_argument('--is_smooth_score', type=bool, default=True,
                        required=False, help='is smooth score')
    parser.add_argument('--is_divided_query', type=bool, default=True,
                        required=False, help='is messages divided query')
    parser.add_argument('--is_keywords_filter', type=bool, default=True,
                        required=False, help='is filter query keyword')
    parser.add_argument('--cuda_num', type=int, default=1,
                        required=False, help='is filter query keyword')
    parser.add_argument('--rec_similarity_cutoff', type=float, default=0.3,
                        required=False, help='recommendation similiarity cutoff')
    parser.add_argument('--faq_similarity_cutoff', type=float, default=0.6,
                        required=False, help='faq similiarity cutoff')
    parser.add_argument('--chunk_similarity_cutoff', type=float, default=0.5,
                        required=False, help='chunk similiarity cutoff')
    parser.add_argument('--message_split', type=int, default=3600,
                        required=False, help='message split')
    parser.add_argument('--query_length', type=int, default=8,
                        required=False, help='query length')
    parser.add_argument('--default_openid', type=str, default="3B697356B6FE29A276291D3311A1C54B",
                        required=False, help='query length')
    parser.add_argument('--is_test', type=bool, default=False,
                        required=False, help='query length')
    parser.add_argument('--index_prefix', type=str, default="",
                        required=False, help='index prefix')
    return parser.parse_args()


args = parse_arguments()

redis_url = args.redis_url
email = args.email
redis_port = args.redis_port
redis_pwd = args.redis_pwd
mysql_url = args.mysql_url
mysql_port = args.mysql_port
mysql_user = args.mysql_user
mysql_pwd = args.mysql_pwd
is_auth_check = args.is_auth_check
is_serial_content = args.is_serial_content
server_location = args.server_location
neo4j_url = args.neo4j_url
neo4j_port = args.neo4j_port
neo4j_user = args.neo4j_user
neo4j_pwd = args.neo4j_pwd
neo4j_db = args.neo4j_db
chunk_size = args.chunk_size
overlap_size = args.overlap_size
db_type = args.db_type
faq_location = args.faq_location
is_smooth_score = args.is_smooth_score
is_divided_query = args.is_divided_query
item_location = args.item_location
character_location = args.character_location
relation_location = args.relation_location
is_keywords_filter = args.is_keywords_filter
cuda_num = args.cuda_num
rec_similarity_cutoff = args.rec_similarity_cutoff
chunk_similarity_cutoff = args.chunk_similarity_cutoff
message_split = args.message_split
query_length = args.query_length
faq_similarity_cutoff = args.faq_similarity_cutoff
default_openid = args.default_openid
is_test = args.is_test
index_prefix = args.index_prefix

indexname_prefix_knowledge = index_prefix + "Chunk"
indexname_prefix_live = index_prefix + "Chunk_Live"
indexname_prefix_relation = index_prefix + "FAQ_Relation"
indexname_prefix_faq = index_prefix + "FAQ"
indexname_prefix_item = index_prefix + "items"

KimiKey = "sk-uCaoqvODQS5FpTiRHxw2ncrjrlR1CNxpuEl6DMx1hny7UEb3"
DeepSeekKey = "sk-6dffdb7a3d8b450a8c0563376f6abe72"

print("redis: {}, {}, {}, {}".format(redis_url, email, redis_port, redis_pwd))
print("mysql: {}, {}, {}".format(mysql_url, mysql_user, mysql_pwd))
print(
    "auth_check: {}, serial_content: {}, server_location: {}, relation_location: {}".format(is_auth_check,
                                                                                            is_serial_content,
                                                                                            server_location,
                                                                                            relation_location))
print("neo4j: {}, {}, {}, {}".format(neo4j_url, neo4j_port, neo4j_user, neo4j_pwd))
print("chunk_size: {}, overlap_size: {}".format(chunk_size, overlap_size))
print("db_type: {}, faq_location: {}, item_location: {}, character_location:{}".format(db_type, faq_location,
                                                                                       item_location,
                                                                                       character_location))
print("is_smooth_score: {}, is_divided_query: {}".format(is_smooth_score, is_divided_query))
print("is_keywords_filter: {}, cuda_num: {}, rec_similiary_cutoff: {}".format(is_keywords_filter, cuda_num,
                                                                              rec_similarity_cutoff))
print("message_split: {}, query_length: {}".format(message_split, query_length))
