#FROM python:3.10 as base
FROM registry.cn-hangzhou.aliyuncs.com/seallhf/neo4j-5.20-commity:cuda

# 将当前目录中的所有文件复制到容器内的 /app 目录中
COPY . /app

# 设置容器的工作目录为 /app
WORKDIR /app

RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y gcc

RUN DEBIAN_FRONTEND=noninteractive apt-get install -y pkg-config
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y libmysqlclient-dev

RUN pip install torch==2.0.1 --index-url https://download.pytorch.org/whl/cpu

# 安装 app.py 所需的依赖，如果有的话
RUN pip install --no-cache-dir -r requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple

# 执行开启镜像运行 app.py
ENTRYPOINT ["python", "./app.py"]
#
#RUN pip install gunicorn==20.1.0
#
## 执行开启镜像运行 Gunicorn
#ENTRYPOINT ["gunicorn", "app:app", "-c", "gunicorn_config.py"]