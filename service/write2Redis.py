import redis

# 创建Redis连接
redis_host = 'llamaindex1.mentlagpt.cc'  # Redis主机
redis_port = 6379  # Redis端口
redis_db = 0  # Redis数据库索引
redis_password = 5228363  # Redis连接密码（如果有的话）

r = redis.Redis(host=redis_host, port=redis_port, db=redis_db, password=redis_password)

# 读取RDB文件的内容
rdb_file_path = 'd:/projects/中职/dump.rdb'  # RDB文件路径
with open(rdb_file_path, 'rb') as file:
    rdb_data = file.read()

# 写入RDB数据到Redis数据库
r.set('rdb_data', rdb_data)

# 关闭Redis连接
r.close()