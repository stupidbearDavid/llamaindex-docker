import os

# alias = ["胡子哥", "孩子", "小胡", "老师", "主播", "哥", "胡子老弟", "虎子哥", "兄弟", "胡总", "大哥", "老板", "小故", "宝宝"]

alias = ["胡子哥", "小胡", "主播", "胡子老弟", "虎子哥", "胡总", "大哥", "老板", "小故"]


def keyword_filter(query):
    filtered_query = query
    for alia in alias:
        filtered_query = filtered_query.replace(alia, "")
    return filtered_query


if __name__ == '__main__':
    print(keyword_filter('哎呀，可能是几个包裹分开发货了。您到快手订单里面，联系下客服给您查查呢。 如果客服给您处理不好，你再联系我。您放心，咱没有收到一定会给您售后处理的，小胡的产品售后不会有问题。'))