# -*- coding: utf-8 -*-
import os
import fnmatch
import json
import traceback

from service.logger import logger
from service.webservice.llamaindex.utils import post_request
from llama_index.vector_stores import RedisVectorStore
from config import redis_url, redis_pwd, redis_port, email, db_type

email_account = email.strip().split(',')

url = "redis://:{}@{}:{}".format(redis_pwd, redis_url, redis_port)

vector_store = None
if db_type == "redis":
    vector_store = RedisVectorStore(
        index_name="llamaindex",
        index_prefix="llamaindex",
        redis_url=url,
        overwrite=False
    )

authority_level = {
    'none': 0,
    'query': 1,
    'pushfile': 2,
    'deletefile': 3,
    'deleteindex': 4,
    'manager': 5,
    'owner': 6
}


async def check_authority(data, operation):
    return True
    # if 'token' in data.keys() or 'oauthToken' in data.keys():
    #     accesstoken = data['token'] if 'token' in data.keys() else data['oauthToken']
    #     email = await get_email(accesstoken) if 'token' in data.keys() else await get_email_oauth(accesstoken)
    #     if 'index_name' in data.keys():
    #         if is_authorized(data['index_name'], email, operation):
    #             return True
    #     else:
    #         return True
    # return False


def validate_input(regex_pattern, input_text):
    if fnmatch.fnmatch(input_text, regex_pattern):
        return True
    else:
        return False


def check_regex_hit(regex_list, input):
    res = [0]
    for regex in regex_list.keys():
        if validate_input(regex, input):
            res.append(regex_list[regex])
    return res


def get_authority_level(authority_regexes, email):
    if email in email_account:
        return authority_level['owner']
    return max(check_regex_hit(authority_regexes, email))


def get_authoritylist():
    if vector_store.client.exists("authority_list"):
        data = vector_store.client.get("authority_list")
        authority_list = json.loads(data)
    else:
        index_list = json.loads(vector_store.client.get("index_list"))
        authority_list = {"llamaindex_managers": {}}
        for key in index_list.keys():
            authority_list[key] = {}
        vector_store.client.append("authority_list", json.dumps(authority_list))
    return authority_list


async def get_email(token):
    try:
        res = await post_request(data=json.dumps({'token': token}))
        if 'email' in res.keys():
            return res['email']
        else:
            return None
    except Exception:
        logger.info(traceback.format_exc())
        return None


async def get_email_oauth(token):
    try:
        res = await post_request(data=json.dumps({'oauthtoken': token}))
        if 'email' in res.keys():
            return res['email']
        else:
            return None
    except Exception:
        return None


def is_authorized(indexname, email, action):
    authority_list = get_authoritylist()
    # if not indexname in authority_list.keys():
    #     logger.info("Node index named [" + indexname + "]!")
    #     return False
    logger.info(indexname, email, action)
    if email in email_account or get_authority_level(authority_list[indexname], email) >= authority_level[action]:
        return True
    if get_authority_level(authority_list["llamaindex_managers"], email) >= authority_level[action]:
        return True
    return False


def get_authorize_level(indexname, email):
    authority_list = get_authoritylist()
    return get_authority_level(authority_list[indexname], email)


def delete_index_regex(indexname, regexs):
    data = get_authoritylist()
    if indexname in data.keys():
        for regex in regexs:
            if regex in data[indexname].keys():
                data[indexname].pop(regex)
        vector_store.client.set("authority_list", json.dumps(data))
        return "Delete regexes from index [" + indexname + "]!"
    else:
        return "Node index named [" + indexname + "]!"


def add_index_regex(indexname, regexes, authlevel):
    data = get_authoritylist()
    if indexname in data.keys():
        for regex in regexes:
            data[indexname][regex] = authlevel

        vector_store.client.set("authority_list", json.dumps(data))
        return "Add regexes authority to index [" + indexname + "] with auth level [" + str(authlevel) + "]!"
    else:
        return "Node index named [" + indexname + "]!"
