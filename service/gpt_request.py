import json
import re
import config
from typing import Any, List
import traceback

from service.logger import logger
from service.webservice.llamaindex.utils import post_request

# gpt_url =
# # gpt_url = 'http://127.0.0.1:3000/api/openai/v1/chat/completions'

'''
data format:
[
    {
        "role": "user",
        "content": "test"
    }
]
'''


async def request_gpt(model: str, messages: Any) -> str:
    payload = {
        'messages': messages,
        # "stream": False,
        'model': model,
        'temperature': 0,
        'presence_penalty': 0,
        'frequency_penalty': 0,
        'top_p': 0
    }
    header = {
        "oauth-token": "e88069d6fbb15ebe3f2ea49740c91ea8",
        "Content-Type": "application/json"
    }
    resJson = await post_request(url='https://mentalgpt.cc/api/openai/v1/chat/completions',
                            headers=header, data=payload)
    pattern = r'(```)([\s\S]*?)(```)'
    matches = re.findall(pattern, resJson['choices'][0]['message']['content'])

    if matches:
        contentInTemplates = [match[1] for match in matches]
        return contentInTemplates[-1].replace('json', '').replace('"```', '').replace('```', '').replace('{{',
                                                                                                         '{').replace(
            '}}', '}')
    else:
        logger.info(resJson['choices'][0]['message']['content'])
        logger.info('没有找到模板字符串')
        return resJson['choices'][0]['message']['content'].replace('```json\n', '').replace('\n```', '').replace('```',
                                                                                                                 '').replace(
            '``', '')


async def request_kimi(model: str, messages=None) -> str:
    # logger.info("[kimi] {}".format(messages))
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer {}".format(config.KimiKey)
    }

    payload = {
        'messages': messages,
        # "stream": False,
        'model': model,
        'temperature': 0.1,
        'presence_penalty': 0,
        'frequency_penalty': 0,
        'top_p': 1.0
    }

    resJson = await post_request(url="https://api.moonshot.cn/v1/chat/completions", headers=headers, data=payload)
    pattern = r'(```)([\s\S]*?)(```)'
    matches = re.findall(pattern, resJson['choices'][0]['message']['content'])

    if matches:
        contentInTemplates = [match[1] for match in matches]
        return contentInTemplates[-1].replace('json', '').replace('"```', '').replace('```', '').replace('{{',
                                                                                                         '{').replace(
            '}}', '}')
    else:
        # logger.info(resJson['choices'][0]['message']['content'])
        logger.info('没有找到模板字符串')
        return resJson['choices'][0]['message']['content'].replace('```json\n', '').replace('\n```', '').replace('```',
                                                                                                                 '').replace(
            '``', '')


async def request_kimi_pure(model: str, messages: Any) -> str:
    # logger.info("[kimi] {}".format(messages))
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer {}".format(config.KimiKey)
    }
    payload = {
        'messages': messages,
        # "stream": False,
        'model': model,
        'temperature': 0.1,
        'presence_penalty': 0,
        'frequency_penalty': 0,
        'top_p': 1.0
    }

    resJson = await post_request(url="https://api.moonshot.cn/v1/chat/completions", headers=headers, data=payload)
    return resJson['choices'][0]['message']['content']


async def request_deepseek(model: str, messages: Any) -> str:
    # logger.info("[deepseek] {}".format(messages))
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer {}".format(config.DeepSeekKey)
    }
    payload = {
        'messages': messages,
        # "stream": False,
        'model': model,
        'temperature': 0.1,
        'presence_penalty': 0,
        'frequency_penalty': 0,
        'top_p': 1.0
    }

    resJson = await post_request(url="https://api.deepseek.com/chat/completions", headers=headers, data=payload)
    pattern = r'(```)([\s\S]*?)(```)'
    matches = re.findall(pattern, resJson['choices'][0]['message']['content'])

    if matches:
        contentInTemplates = [match[1] for match in matches]
        return contentInTemplates[-1].replace('json', '').replace('"```', '').replace('```', '').replace('{{',
                                                                                                         '{').replace(
            '}}', '}')
    else:
        # logger.info(resJson['choices'][0]['message']['content'])
        logger.info('没有找到模板字符串')
        return resJson['choices'][0]['message']['content'].replace('```json\n', '').replace('\n```', '').replace('```',
                                                                                                                 '').replace(
            '``', '')


async def request_deepseek_pure(model: str, messages: Any) -> str:
    logger.info("[deepseek] {}".format(messages))
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer {}".format(config.DeepSeekKey)
    }
    payload = {
        'messages': messages,
        # "stream": False,
        'model': model,
        'temperature': 0.001,
        'presence_penalty': 0,
        'frequency_penalty': 0,
        'top_p': 0.1
    }

    resJson = await post_request(url="https://api.deepseek.com/chat/completions", headers=headers, data=payload)
    return resJson['choices'][0]['message']['content']


async def request_llama3_pure(messages: Any) -> str:
    logger.info("[llama3] {}".format(messages))
    headers = {
        "Content-Type": "application/json",
        # "Authorization": "Bearer {}".format(config.DeepSeekKey)
    }
    payload = {
        'messages': messages,
    }

    resJson = await post_request(url="http://192.168.31.221:8888/chat", headers=headers, data=payload)
    return resJson


def get_baidu_url(model: str) -> str:
    if model == "ERNIE-Bot-turbo":
        return 'https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/eb-instant?access_token='
    elif model == "ERNIE-Bot":
        return 'https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/completions?access_token='
    elif model == "Llama2-70b":
        return 'https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/llama_2_70b?access_token='
    elif model == "Llama2-13b":
        return 'https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/llama_2_13b?access_token='
    elif model == "Llama2-7b":
        return 'https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/llama_2_7b?access_token='
    elif model == "Stable-Diffusion-XL":
        return 'https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/text2image/sd_xl?access_token='
    elif model == "ERNIE-Bot-4.0":
        return 'https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/completions_pro?access_token='


async def get_access_token() -> str:
    url = 'https://aip.baidubce.com/oauth/2.0/token?grant_type=client_credentials&client_id=' + config.AK + '&client_secret=' + config.SK
    options = {
        "method": "POST",
    }
    try:
        res_dict = await post_request(url=url, data=options)
        return res_dict.get('access_token')
    except Exception as e:
        logger.info(f"Error occurred: {str(e)}")
        return ""


async def request_enire(input_messages: Any, temperature: float, top_p: float):
    access_token = None
    try:
        access_token = await get_access_token()
    except Exception as e:
        logger.info("[Baidu] failed to get access token, {}".format(e))

    requestData = {
        "messages": input_messages,
        "model": "ERNIE-Bot",
        "temperature": max(temperature, 0.001),
        "penalty_score": 1.0,
        "top_p": top_p,
        "disable_search": True,
        "stream": False
    }

    systemMessage = '\n'.join([n["content"] for n in input_messages if n["role"] == "system"])
    messages = [m for m in input_messages if m["role"] != "system"]
    cloned_body = {
        **requestData,
        "messages": [],
        "system": systemMessage if systemMessage else ""
    }

    for i in range(len(messages)):
        if messages[i]["content"] == "":
            continue
        if not cloned_body["messages"]:
            if messages[i]["role"] == "user":
                cloned_body["messages"].append(messages[i])
            else:
                cloned_body["messages"].append({"content": "\n", "role": "user"})
                cloned_body["messages"].append(messages[i])
        else:
            if messages[i]["role"] != cloned_body["messages"][-1]["role"]:
                cloned_body["messages"].append(messages[i])
            else:
                cloned_body["messages"][-1]["content"] += '\n\n' + messages[i]["content"]

    chat_payload = {
        "method": "POST",
        "body": json.dumps(cloned_body, ensure_ascii=False),
        "headers": {
            "Content-Type": "application/json",
        }
    }

    url = get_baidu_url(cloned_body["model"]) + access_token

    try:
        res_json = await post_request(url=url, data=chat_payload)
        pattern = '(```)([\s\S]*?)(```)'
        matches = re.findall(pattern, res_json["result"].replace('\n', ''))

        if matches:
            content_in_templates = [match[1] for match in matches]
            return content_in_templates[-1].replace('json', '').replace('```', '')
        else:
            logger.info(res_json["result"])
            logger.info("没有找到模板字符串")
            return res_json["result"].replace('json', '').replace('```', '')
    except Exception:
        logger.info(traceback.format_exc())


async def get_entity(prompt):
    result = await request_kimi(model="moonshot-v1-8k", messages=prompt)
    logger.info("[kimi result] {}".format(result))
    return result


async def main(data):
    # data = "如果汽车空调管道没有定期清洁，异味是很容易出现的。更换空调滤芯不能根本解决问题，只能通过免拆清洗和拆解清洗来清洁空调管道。"
    prompt = [{"role": "user", "content": "帮我提取文本内容中的汽车部件：“{}”，只回复汽车配件部件名称：".format(data)}]

    result = await get_entity(prompt)

    return result


if __name__ == '__main__':
    import asyncio
    #
    # output = open("D:/zhouying/培训内容/车辆故障情况_entity.txt", "w", encoding="utf-8")
    # with open("D:/zhouying/培训内容/车辆故障情况.txt", encoding="utf-8") as data:
    #     i = 0
    #     for line in data:
    #         output.write(line)
    #         if i % 2 != 0:
    #             result = asyncio.run(main(line))
    #             output.write(result + '\n')
    #         i += 1
    #
    # output.close()

    result = asyncio.run(request_llama3_pure([{"role": "user", "content": "你好"}]))
    print(result)