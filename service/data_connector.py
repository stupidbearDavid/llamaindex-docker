from llama_index import ListIndex, SimpleWebPageReader, TreeIndex, GPTTreeIndex, Document
import requests
from bs4 import BeautifulSoup


def remove_tags_from_page(url):
    # 发送GET请求获取页面内容
    response = requests.get(url)

    # 使用BeautifulSoup解析页面内容
    soup = BeautifulSoup(response.text, 'html.parser')

    # 删除所有标签
    for tag in soup.find_all():
        tag.extract()

    # 返回删除标签后的纯文本内容
    return soup.get_text()


def get_content_from_webpage(urls):
    # documents = []
    # for url in urls:
    #     documents.append(Document(text=remove_tags_from_page(url)))

    documents = SimpleWebPageReader(html_to_text=True).load_data(urls)
    print(documents[0].text)


if __name__ == '__main__':
    get_content_from_webpage(["https://baike.baidu.com/item/%E8%82%96%E6%88%98/18866899?fromModule=lemma_search-box"])