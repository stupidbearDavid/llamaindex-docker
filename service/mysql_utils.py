import struct
import os
import ast
import json
import MySQLdb
import numpy as np
from typing import Any, List

from service.logger import logger
from service.redis_index import get_embedding_feature
from config import mysql_pwd, mysql_url, mysql_user, mysql_port


def cosine_similarity(v1, v2):
    dot_product = np.dot(v1, v2)
    norm_v1 = np.linalg.norm(v1)
    norm_v2 = np.linalg.norm(v2)

    similarity = dot_product / (norm_v1 * norm_v2)

    return similarity


def bytes_to_float_array(byte_array):
    float_array = []
    num_bytes = len(byte_array) // 4  # 每个float占用4个字节

    for i in range(num_bytes):
        float_value = struct.unpack('f', byte_array[i*4:(i+1)*4])[0]
        float_array.append(float_value)

    return float_array


def user_cache_check(wx_id: str, open_id: str, rec_content: Any) -> Any:
    db_connection = MySQLdb.connect(
        host=mysql_url,
        user=mysql_user,
        port=mysql_port,
        passwd=mysql_pwd,
        db="llm_private",
        charset='utf8mb4'
    )
    cursor = db_connection.cursor()

    try:
        logger.info("[update cache] {}, {}, {}".format(wx_id, open_id, rec_content))
        # 检查是否存在记录
        cursor.execute("SELECT recent_rec FROM user_itemRec_cache WHERE wxId = %s AND userId = %s", (wx_id, open_id))
        result = cursor.fetchone()

        checked_dimensions = {}

        if result:
            # result = json.loads(result[0])
            result = ast.literal_eval(result[0])
            for key in rec_content.keys():
                if key in result.keys() and rec_content[key] == result[key]:
                    continue
                checked_dimensions[key] = rec_content[key]
            # 如果存在，更新 update_cache 字段
            cursor.execute("UPDATE user_itemRec_cache SET recent_rec = %s WHERE wxId = %s AND userId = %s",
                           (rec_content, wx_id, open_id))
        else:
            # 如果不存在，插入新记录
            cursor.execute("INSERT INTO user_itemRec_cache (wxId, userId, recent_rec) VALUES (%s, %s, %s)",
                           (wx_id, open_id, json.dumps(rec_content, ensure_ascii=False)))
            checked_dimensions = rec_content

        # 提交事务
        db_connection.commit()
        return checked_dimensions

    except Exception as e:
        # 回滚事务
        db_connection.rollback()
        print(f"An error occurred: {e}")
        return rec_content

    finally:
        # 关闭数据库连接
        cursor.close()
        db_connection.close()