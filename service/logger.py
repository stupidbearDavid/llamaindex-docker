from config import root_path

import logging
from logging.handlers import TimedRotatingFileHandler
import time

# 创建Logger对象
logger = logging.getLogger('llamaindex_faq')
logger.setLevel(logging.DEBUG)

# 创建按日期切割的文件处理程序
log_filename = root_path + 'logs/rag.log'  # 基础文件名，不包含日期
# 每天切割一个日志文件，保留7个旧日志文件
file_handler = TimedRotatingFileHandler(log_filename, when='midnight', interval=1, backupCount=7, encoding='utf-8')
file_handler.suffix = "%Y-%m-%d"  # 只包含日期信息
file_handler.setLevel(logging.DEBUG)

# 创建控制台处理程序
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)

# 创建日志格式器
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
file_handler.setFormatter(formatter)
console_handler.setFormatter(formatter)

# 添加处理程序到Logger
logger.addHandler(file_handler)
logger.addHandler(console_handler)

# 记录日志信息
# logger.debug('This is a debug message')
# logger.info('This is an info message')
# logger.warning('This is a warning message')
# logger.error('This is an error message')
# logger.critical('This is a critical message')
