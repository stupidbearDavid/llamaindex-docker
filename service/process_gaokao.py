import pandas as pd
import os
import requests
import json
import struct
import numpy as np


def cosine_similarity(v1, v2):
    dot_product = np.dot(v1, v2)
    norm_v1 = np.linalg.norm(v1)
    norm_v2 = np.linalg.norm(v2)

    similarity = dot_product / (norm_v1 * norm_v2)

    return similarity


def rewrite_data():
    data = open('D:/projects/中职/高考/QA.txt', 'w', encoding="utf-8")
    n = 0
    for _, _, files in os.walk("D:/projects/中职/高考/excel/"):
        for f in files:
            try:
                # 读取xlsx文件
                df = pd.read_excel("D:/projects/中职/高考/excel/" + f)
                content = df.values[:, 1]
                # 打印数据框的内容
                for i in range(len(content) // 2):
                    data.write(content[i * 2] + '\t' + content[i * 2 + 1] + '\n')
            except Exception:
                print(f)
            n += 1
            print(n, '/', len(files))

    data.close()


def write2Server(content):
    url = 'https://llamaindex.mentalgpt.cc/content'  # 替换为你要POST的URL
    data = {"indexname": "高考咨询", "oauthToken": "e88069d6fbb15ebe3f2ea49740c91ea8", "text": content}  # 替换为你要发送的POST数据
    json_data = json.dumps(data)
    response = requests.post(url, headers={"Content-Type": "application/json"}, data=json_data)

    if response.status_code == 200:
        # print('POST请求成功！')
        print('响应内容：', response.content)
    else:
        print('POST请求失败！状态码：', response.content)


def create_school_section():

    data = open('D:/projects/中职/高考/QA.txt', encoding="utf-8")
    output = open('D:/projects/中职/高考/QA_schools.txt', 'w', encoding='utf-8')
    for line in data:
        question = line.split('\t')[0]
        if question.startswith("可以介绍一下"):
            school = question.split("可以介绍一下")[1].split('的')[0]
        else:
            school = question.split('的')[0]
        output.write(school + '\t' + line)
    output.close()


def get_school_names():
    data = open('D:/projects/中职/高考/QA_schools.txt', encoding="utf-8")
    output = open('D:/projects/中职/高考/schools.txt', 'w', encoding='utf-8')
    schools = []
    for line in data:
        school = line.split('\t')[0]
        if school not in schools:
            schools.append(school)

    for school in schools:
        output.write(school + '\n')

    output.close()


def float_array_to_bytes(float_array):
    byte_array = b''
    for num in float_array:
        byte_array += struct.pack('f', num)
    return byte_array


def bytes_to_float_array(byte_array):
    float_array = []
    num_bytes = len(byte_array) // 4  # 每个float占用4个字节

    for i in range(num_bytes):
        float_value = struct.unpack('f', byte_array[i*4:(i+1)*4])[0]
        float_array.append(float_value)

    return float_array


def write2mysql():
    import MySQLdb
    from service.redis_index import get_embedding_feature, float_array_to_bytes

    # 连接到 MySQL 数据库
    db_connection = MySQLdb.connect(
        host="localhost",
        user="root",
        passwd="5228363",
        db="zz",
        charset='utf8mb4'
    )
    cursor = db_connection.cursor()

    data = open('D:/projects/中职/高考/QA_schools.txt', encoding="utf-8")

    i = 0

    for line in data:
        try:
            if i <= 149696:
                i += 1
                continue
            school = line.split('\t')[0]
            question = line.split('\t')[1]
            answer = line.split('\t')[2].strip()
            feature = float_array_to_bytes(get_embedding_feature(question))
            cursor.execute("INSERT INTO qa (school_name, question, answer, feature) VALUES (%s, %s, %s, %s)",
                           (school, question, answer, feature))
            print(i, school, question, answer)
            i += 1
            # 提交更改
            db_connection.commit()
        except Exception:
            print(i, line.split('\t')[0], line.split('\t')[1])

    # 关闭数据库连接
    cursor.close()
    db_connection.close()


def query(school, question):
    import MySQLdb
    from service.redis_index import get_embedding_feature, bytes_to_float_array

    # 连接到 MySQL 数据库
    db_connection = MySQLdb.connect(
        host="llamaindex.mentalgpt.cc",
        user="root",
        passwd="5228363",
        db="zz",
        charset='utf8mb4'
    )
    cursor = db_connection.cursor()

    try:
        # 查询符合条件的数据
        query = "SELECT * FROM qa WHERE school_name like %s"

        cursor.execute(query, (school, ))
        results = cursor.fetchall()

        q_feature = np.array(get_embedding_feature(question))
        similiarties = []
        # 输出查询结果
        for row in results:
            feature = np.array(bytes_to_float_array(row[3]))
            similiarties.append([cosine_similarity(q_feature, feature), row[2], row[4]])
            # print(feature)  # 可以根据实际需要对查询结果进行其他处理
        print(similiarties)
        sorted_arr = sorted(similiarties, key=lambda x: x[0], reverse=True)
        print(sorted_arr[0])

    except MySQLdb.Error as e:
        print("Error executing query: {}".format(e))

    finally:
        # 关闭游标和数据库连接
        cursor.close()
        db_connection.close()


if __name__ == '__main__':
    # data = open('D:/projects/中职/高考/QA.txt', encoding="utf-8")
    # i = 0
    # for line in data:
    #     if i < 1060:
    #         i += 1
    #         continue
    #     question = line.split('\t')[0]
    #     print(question)
    #     write2Server(question)

    # create_school_section()
    # get_school_names()
    write2mysql()
    # query("三峡大学", "信息管理专业怎么样啊？")

    # test = [1231.341, 123.124, 12.31]
    #
    # print(float_array_to_bytes(test))
    # print(bytes_to_float_array(float_array_to_bytes(test)))