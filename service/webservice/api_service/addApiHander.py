#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
import traceback

from aiohttp import web

from service.apiIndcing import add_api_index
from service.authority import get_email, get_email_oauth, is_authorized

root_path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + os.path.sep + ".") + '/'


def create_api(api_name: str, method: str, api_url: str, params: dict, api_description: str, headers: dict):
    error_params = {}
    for key in params.keys():
        if "field_name" not in params[key].keys():
            error_params[key] = ["field_name"]
        if "description" not in params[key].keys():
            if key in error_params.keys():
                error_params[key].append("description")
            else:
                error_params[key] = ["description"]
    if len(error_params.keys()) > 0:
        return False, error_params
    return True, {
        "name": api_name,
        "method": method,
        "api_url": api_url,
        "params": params,
        "description": api_description,
        "headers": headers
    }


async def AddIndexRequest(request):
    try:
        data = await request.json()
    except json.JSONDecodeError:
        return web.Response(text="Invalid JSON", status=400)
    try:
        api_name = data['api']
        api_url = data['api_url']
        method = data['method']
        business = data['business']
        headers = data['headers'] if "headers" in data.keys() else None
        if 'token' in data.keys() or 'oauthToken' in data.keys():
            accesstoken = data['token'] if 'token' in data.keys() else data['oauthToken']
            email = await get_email(accesstoken) if 'token' in data.keys() else await get_email_oauth(accesstoken)
            if is_authorized(business, email, 'pushfile'):
                description = data['description']
                params = data['params']
                is_pass_check, chunk = create_api(api_name, method, api_url, params, description, headers)
                if is_pass_check:
                    response = add_api_index(chunk, business)
                    response = json.loads(json.dumps(response, ensure_ascii=False))
                    return web.Response(status=200, text=response["message"])
                else:
                    return web.Response(status=200, text="parameter profile info is missing: {}".format(chunk))
            else:
                response = {
                    'status': 401,
                    'message': "Authority deny!"
                }
                return web.json_response(status=401, data=response)
        else:
            response = {
                'status': 401,
                'message': "Authority deny!"
            }
            return web.json_response(status=401, data=response)
    except Exception:
        print(traceback.format_exc())
        response = {
            'status': 500,
            'message': traceback.format_exc()
        }
        return web.json_response(status=500, data=response)

