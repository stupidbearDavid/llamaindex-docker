#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
import traceback

from aiohttp import web, ClientSession

from config import is_auth_check
from llama_index.vector_stores.types import VectorStoreQueryMode
from service.apiIndcing import switch_business
from service.authority import get_email, get_email_oauth, is_authorized

root_path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + os.path.sep + ".") + '/'

API_STATUS = {
    "ok": 200,
    "lack_param": 200,
    "error": 403
}


def type_trans(data, type):
    if type == "int":
        return int(data)
    elif type == "float":
        return float(data)
    elif type == "bool":
        return data.lower() == "true"
    else:
        return str(data)


def params_check(already_knows, params, query) -> []:
    lack_params = []
    payload_params = {"query": query}
    for k in params.keys():
        key = params[k]["field_name"]
        if key not in already_knows.keys():
            if "default" not in params[k].keys():
                lack_params.append(params[k]["description"])
            else:
                payload_params[k] = params[k]["default"]
        else:
            if "type" not in params[k].keys():
                payload_params[k] = already_knows[key]
            else:
                payload_params[k] = type_trans(already_knows[key], params[k]["type"])
    return lack_params, payload_params


async def request_api(api_profile, already_knows, query):
    try:
        method = api_profile["method"]
        params = api_profile["params"]
        api_url = api_profile["api_url"]
        headers = api_profile["headers"] if "headers" in api_profile.keys() else None
        lack_params, payload_params = params_check(already_knows, params, query)
        if len(lack_params) > 0:
            return {
                "api_status": "lack_param",
                "message": ",".join(lack_params)
            }
        else:
            if method == "post":
                async with ClientSession() as session:
                    async with session.post(api_url, headers=headers, json=payload_params) as response:
                        res = await response.text()
                        if response.status != 200:
                            return {
                                "api_status": "error",
                                "message": res
                            }
                        else:
                            return {
                                "api_status": "ok",
                                "message": res
                            }
            elif method == "get":
                async with ClientSession() as session:
                    async with session.get(api_url, headers=headers, params=payload_params) as response:
                        res = await response.text()
                        if response.status != 200:
                            return {
                                "api_status": "error",
                                "message": res
                            }
                        else:
                            return {
                                "api_status": "ok",
                                "message": res
                            }
            else:
                return {
                    "api_status": "error",
                    "message": "Bad request method: " + method
                }
    except Exception:
        print(traceback.format_exc())
        return {
            "api_status": "error",
            "message": traceback.format_exc()
        }


async def query(data):
    try:
        if 'token' in data.keys() or 'oauthToken' in data.keys():
            accesstoken = data['token'] if 'token' in data.keys() else data['oauthToken']
            email = await get_email(accesstoken) if 'token' in data.keys() else await get_email_oauth(accesstoken)
            business = data["business"]
            already_know = data['already_know']

            if is_authorized(business, email, 'query'):
                if "similarity_cutoff" in data.keys():
                    similarity_cutoff = data["similarity_cutoff"]
                else:
                    similarity_cutoff = 0.4
                if "similarity_top_k" in data.keys():
                    similarity_top_k = data["similarity_top_k"]
                else:
                    similarity_top_k = 1
                INDEX = switch_business(business)

                engine = INDEX.as_query_engine(vector_store_query_mode=VectorStoreQueryMode.DEFAULT,
                                               similarity_top_k=similarity_top_k,
                                               vector_store_kwargs={'similarity_cutoff': similarity_cutoff})
                query = data["query"].split('\n')[-1]
                # query based on Api-meta
                search_result = engine.query(query).response
                if len(search_result) > 0:
                    api_profile = json.loads(search_result[0])
                    response = await request_api(api_profile, already_know, query)
                    response = json.loads(json.dumps(response, ensure_ascii=False))
                    return web.json_response(status=200, data=response)
                else:
                    response = {
                        'status': 404,
                        'message': ""
                    }
                    return web.json_response(status=404, data=response)
            else:
                response = {
                    'status': 401,
                    'message': "Authority deny!"
                }
                return web.json_response(status=401, data=response)
        else:
            response = {
                'status': 401,
                'message': "Authority deny!"
            }
            return web.json_response(status=401, data=response)
    except Exception:
        response = {
            'status': 500,
            'message': traceback.format_exc()
        }
        return web.json_response(status=500, data=response)


async def query_without_auth(data):
    try:
        business = data["business"]
        already_know = data['already_know']

        if "similarity_cutoff" in data.keys():
            similarity_cutoff = data["similarity_cutoff"]
        else:
            similarity_cutoff = 0.4
        if "similarity_top_k" in data.keys():
            similarity_top_k = data["similarity_top_k"]
        else:
            similarity_top_k = 1
        INDEX = switch_business(business)

        engine = INDEX.as_query_engine(vector_store_query_mode=VectorStoreQueryMode.DEFAULT,
                                       similarity_top_k=similarity_top_k,
                                       vector_store_kwargs={'similarity_cutoff': similarity_cutoff})
        query = data["query"].split('\n')[-1]
        # query based on Api-meta
        search_result = engine.query(query).response
        if len(search_result) > 0:
            api_profile = json.loads(search_result[0])
            response = await request_api(api_profile, already_know, query)
            response = json.loads(json.dumps(response, ensure_ascii=False))
            return web.json_response(status=200, data=response)
        else:
            response = {
                'status': 404,
                'message': ""
            }
            return web.json_response(status=404, data=response)
    except Exception:
        response = {
            'status': 500,
            'message': traceback.format_exc()
        }
        return web.json_response(status=500, data=response)


async def QueryApiRequest(request):
    try:
        data = await request.json()
    except json.JSONDecodeError:
        return web.Response(text="Invalid JSON", status=400)
    try:
        if is_auth_check:
            return await query(data)
        else:
            return await query_without_auth(data)
    except Exception:
        print(traceback.format_exc())
        response = {
            'status': 500,
            'message': traceback.format_exc()
        }
        return web.json_response(status=500, data=response)
