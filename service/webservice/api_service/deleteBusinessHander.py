#!/usr/bin/env python
# -*- coding: utf-8 -*-

from aiohttp import web

import os
import json
import traceback
from service.apiIndcing import delete_business
from service.authority import get_email, get_email_oauth, is_authorized


async def DeleteBusinessRequest(request):
    try:
        data = await request.json()
    except json.JSONDecodeError:
        return web.Response(text="Invalid JSON", status=400)
    try:
        business = data['business']
        if 'token' in data.keys() or 'oauthToken' in data.keys():
            accesstoken = data['token'] if 'token' in data.keys() else data['oauthToken']
            email = await get_email(accesstoken) if 'token' in data.keys() else await get_email_oauth(accesstoken)
            if is_authorized(business, email, 'deleteindex'):
                delete_business(business)
                response = {
                    'status': 200,
                    'message': f"Business with name {business} deleted successfully."
                }
                response = json.loads(json.dumps(response, ensure_ascii=False))
                return web.json_response(status=200, data=response)
            else:
                response = {
                    'status': 401,
                    'message': "Authority deny!"
                }
                return web.json_response(status=401, data=response)
        else:
            response = {
                'status': 401,
                'message': "Authority deny!"
            }
            return web.json_response(status=401, data=response)
    except Exception:
        print(traceback.format_exc())
        response = {
            'status': 500,
            'message': traceback.format_exc()
        }
        return web.json_response(status=500, data=response)

