#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
import traceback

from aiohttp import web

# from service.mysql_utils import query_school_rec

root_path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + os.path.sep + ".") + '/'


async def SchoolRecRequest(request):
    try:
        data = await request.json()
    except json.JSONDecodeError:
        return web.Response(text="Invalid JSON", status=400)
    try:
        score = data["score"]
        province = data["province"]
        major = data["major"]
        expectation = data["expectation"]

        # school_recs = query_school_rec({"province": province, "score": score, "major": major,
        #                                 "expectation": expectation})
        # return web.Response(text=school_recs)
    except Exception:
        print(traceback.format_exc())
        response = {
            'status': 500,
            'message': traceback.format_exc()
        }
        return web.json_response(status=500, data=response)