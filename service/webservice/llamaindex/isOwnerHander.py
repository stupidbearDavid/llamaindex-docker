#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
import traceback

from aiohttp import web

from service.authority import get_email, get_authorize_level, authority_level

root_path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + os.path.sep + ".") + '/'


async def IsOwnerRequest(request):
    try:
        data = await request.json()
    except json.JSONDecodeError:
        return web.Response(text="Invalid JSON", status=400)
    try:
        if 'token' in data.keys():
            accesstoken = data['token']
            email = await get_email(accesstoken)
            if get_authorize_level("llamaindex_managers", email) == authority_level['owner']:
                return web.json_response(status=200, data={'type': 1})
            else:
                return web.json_response(status=200, data={'type': 0})
        else:
            response = {
                'status': 401,
                'message': "Authority deny!"
            }
            return web.json_response(status=401, data=response)
    except Exception:
        print(traceback.format_exc())
        response = {
            'status': 500,
            'message': traceback.format_exc()
        }
        return web.json_response(status=500, data=response)