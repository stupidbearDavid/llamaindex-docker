#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
import traceback

from aiohttp import web

from service.logger import logger
from service.indcing import add_content_index
from service.authority import check_authority

root_path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + os.path.sep + ".") + '/'


async def ContentRequest(request):
    try:
        data = await request.json()
    except json.JSONDecodeError:
        return web.Response(text="Invalid JSON", status=400)
    try:
        index_name = data['indexname']
        logger.info('add content: {}'.format(data))
        if check_authority(data, 'pushfile'):
            text = data['content']
            if 'filename' in data.keys():
                filename = data['filename']
            else:
                filename = None
            res = await add_content_index(indexname=index_name, content=text, filename=filename)
            response = {
                'status': 200,
                'message': res
            }
            response = json.loads(json.dumps(response, ensure_ascii=False))
            return web.json_response(status=200, data=response)
        else:
            response = {
                'status': 401,
                'message': "Authority deny!"
            }
            return web.json_response(status=401, data=response)

    except Exception:
        print(traceback.format_exc())
        response = {
            'status': 500,
            'message': traceback.format_exc()
        }
        return web.json_response(status=500, data=response)
