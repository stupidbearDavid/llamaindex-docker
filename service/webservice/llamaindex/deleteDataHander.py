#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import json
import traceback

from aiohttp import web

from service.logger import logger
from service.authority import get_email, is_authorized
from service.neo4j_index import delete_index_QA

root_path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + os.path.sep + ".") + '/'


async def DeleteDataRequest(request):
    try:
        data = await request.json()
    except json.JSONDecodeError:
        return web.Response(text="Invalid JSON", status=400)
    try:
        logger.info('delete data: {}'.format(data))
        business = data['business']
        unique_id = data['unique_id']

        delete_index_QA(business, unique_id)

        response = {
            'status': 200,
            'message': "Clear data: [{}: {}]!".format(business, unique_id)
        }
        return web.json_response(status=200, data=response)
    except Exception:
        logger.error(traceback.format_exc())
        response = {
            'status': 500,
            'message': 'Interval Error!'
        }
        return web.json_response(status=500, data=response)