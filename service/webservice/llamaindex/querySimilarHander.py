#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
import traceback

from aiohttp import web

import numpy as np
from service.logger import logger
from service.model import Singleton

root_path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + os.path.sep + ".") + '/'


def cosine_similarity(vector1, vector2):
    """
    计算两个向量之间的余弦相似度

    参数:
    vector1 (list or np.array): 第一个向量
    vector2 (list or np.array): 第二个向量

    返回:
    float: 余弦相似度值，范围在[-1, 1]之间
    """
    # 将输入向量转换为numpy数组
    vector1 = np.array(vector1)
    vector2 = np.array(vector2)

    # 计算向量的点积
    dot_product = np.dot(vector1, vector2)

    # 计算向量的模（范数）
    norm_vector1 = np.linalg.norm(vector1)
    norm_vector2 = np.linalg.norm(vector2)

    # 计算余弦相似度
    cosine_sim = dot_product / (norm_vector1 * norm_vector2)

    return cosine_sim


async def QuerySimilarRequest(request):
    try:
        data = await request.json()
    except json.JSONDecodeError:
        return web.Response(text="Invalid JSON", status=400)
    try:
        passages = data["passages"]
        response = Singleton.instance().instance["embedding_model"].embed_documents(passages)
        response = {
            "score": cosine_similarity(response[0], response[1])
        }
        return web.json_response(data=response, status=200)
    except Exception:
        logger.error(traceback.format_exc())
        response = {
            'status': 500,
            'message': traceback.format_exc()
        }
        return web.json_response(status=500, data=response)
