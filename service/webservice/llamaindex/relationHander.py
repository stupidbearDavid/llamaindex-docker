#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
import traceback

from aiohttp import web

from service.logger import logger
from service.indcing import add_relation_index
from service.authority import check_authority


"""
request structure:
{
    "from": {
        "from_label": "人物",
        "from_props": {"name": "胡子哥"},
        "from_key": "name"
    },
    "to": {
        "to_label": "人物",
        "to_props": {"name": "苏超群"},
        "to_key": "name"
    },
    "relationship":"哥哥"
}
"""
async def RelationRequest(request):
    try:
        data = await request.json()
    except json.JSONDecodeError:
        return web.Response(text="Invalid JSON", status=400)
    try:
        from_vertex = data['from']
        to_vertex = data['to']
        logger.info("add relation: {}".format(data))
        if check_authority(data, 'pushfile'):
            # from_label, from_props, from_key, to_label, to_props, to_key, relationship
            res = add_relation_index(from_label=from_vertex["from_label"], from_props=from_vertex["from_props"],
                                     from_key=from_vertex["from_key"], to_label=to_vertex["to_label"],
                                     to_props=to_vertex["to_props"], to_key=to_vertex["to_key"],
                                     relationship=data["relationship"])
            response = {
                'status': 200,
                'message': res
            }
            response = json.loads(json.dumps(response, ensure_ascii=False))
            return web.json_response(status=200, data=response)
        else:
            response = {
                'status': 401,
                'message': "Authority deny!"
            }
            return web.json_response(status=401, data=response)

    except Exception:
        logger.info(traceback.format_exc())
        response = {
            'status': 500,
            'message': 'Interval Error!'
        }
        return web.json_response(status=500, data=response)
