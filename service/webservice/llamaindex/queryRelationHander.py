#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
import traceback

from aiohttp import web

import time
from config import is_auth_check, db_type, is_divided_query, is_keywords_filter, is_smooth_score, \
    faq_similarity_cutoff, query_length, default_openid, cuda_num, is_test, indexname_prefix_relation
from service.keywords_filter import keyword_filter
from service.logger import logger
from service.model import Singleton
from service.indcing import switch_index, index_query
from service.authority import check_authority
from service.gpt_request import request_gpt
from service.neo4j_index import score_smooth
from service.webservice.llamaindex.utils import get_kol_id

root_path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + os.path.sep + ".") + '/'


def rerank_response(query, answers, similarity_cutoff):
    rerank_results = Singleton.instance().rerank(query, answers)
    # rerank_results = indexStore.reranker_model.rerank(query, answers)
    filtered_scores = [x for x in rerank_results["rerank_scores"] if x >= similarity_cutoff]
    result = []
    for i in range(len(filtered_scores)):
        result.append([rerank_results["rerank_passages"][i], rerank_results["rerank_scores"][i]])
    return result


async def request_faq(query_ori, details, index_name, metadata, is_rerank, keywords_filter, similarity_cutoff,
                      similarity_top_k):
    if keywords_filter:
        query = keyword_filter(query_ori)
    else:
        query = query_ori
    INDEX = switch_index(index_name)

    qa_query_type = "query_type" in metadata.keys() and metadata["query_type"] == "QA"

    if is_rerank:
        passages = await index_query(INDEX, query, 10, 0.2, metadata)
        for p in range(len(passages)):
            passages[p][2] = score_smooth(score=passages[p][2]) if is_smooth_score else passages[p][2]
        if qa_query_type:
            details.append(
                json.dumps({"name": "query_qa", "query": query, "result": str(passages)}, ensure_ascii=False))
            # details.append(
            #     json.dumps({"name": "query_qa", "query": query}, ensure_ascii=False))
            if db_type == "redis":
                passage_dict = {p[0]: p[1:] for p in passages}
                response_query = rerank_response(query, passage_dict.keys(), similarity_cutoff)[
                                 :similarity_top_k]
                response_str = []
                for k in response_query:
                    # response_str.append(
                    #     {"username": k[0], "relation": passage_dict[k[0]][0], "score": passage_dict[k[0]][1],
                    #      "threshold": passage_dict[k[0]][2]["threshold"]})
                    # response_str.append(
                    #     {"username": k[0], "relation": passage_dict[k[0]][0], "score": passage_dict[k[0]][1]})
                    response_str.append(passage_dict[k[0]][0])
            elif db_type == "neo4j":
                response_query = rerank_response(query, passages.keys(),
                                                 similarity_cutoff)[
                                 :similarity_top_k]
                response_str = []
                for k in response_query:
                    # response_str.append({"username": k[0], "relation": passages[k[0]][0], "score": passages[k[0]][1],
                    #                      "threshold": passages[k[0]][2]})
                    # response_str.append({"username": k[0], "relation": passages[k[0]][0], "score": passages[k[0]][1]})
                    response_str.append(passages[k[0]][0])
            else:
                passage_dict = {p[0]: p[1:] for p in passages}
                response_query = rerank_response(query, passage_dict.keys(), similarity_cutoff)[
                                 :similarity_top_k]
                response_str = []
                for k in response_query:
                    # response_str.append(
                    #     {"username": k[0], "relation": passage_dict[k[0]][0], "score": passage_dict[k[0]][1],
                    #      "threshold": passage_dict[k[0]][2]["threshold"]})
                    # response_str.append(
                    #     {"username": k[0], "relation": passage_dict[k[0]][0], "score": passage_dict[k[0]][1]})
                    response_str.append(passage_dict[k[0]][0])
            details.append(json.dumps({"name": "reranked", "result": str(response_str)}, ensure_ascii=False))
            # details.append(json.dumps({"name": "reranked"}, ensure_ascii=False))
        else:
            details.append(
                json.dumps({"name": "query_chunk", "query": query, "result": str(passages)}, ensure_ascii=False))
            # details.append(
            #     json.dumps({"name": "query_chunk", "query": query}, ensure_ascii=False))
            response = rerank_response(query, passages, similarity_cutoff)[
                       :similarity_top_k]
            response_str = []
            for k in response:
                # response_str.append({"username": k[0], "relation": passages[k[0]][0], "score": passages[k[0]][1],
                #                      "threshold": passages[k[0]][2]})
                # response_str.append({"username": k[0], "relation": passages[k[0]][0], "score": passages[k[0]][1]})
                response_str.append(passages[k[0]][0])
            details.append(json.dumps({"name": "reranked", "result": str(response_str)}, ensure_ascii=False))
            # details.append(json.dumps({"name": "reranked"}, ensure_ascii=False))
    else:
        passages = await index_query(INDEX, query, similarity_top_k,
                                     similarity_cutoff, metadata)
        # for p in range(len(passages)):
        #     passages[p][2] = score_smooth(score=passages[p][2]) if is_smooth_score else passages[p][2]
        response_str = []
        for k in passages:
            response_str.append(k[1])
        details.append(json.dumps({"name": "query_chunk", "query": query, "result": str(passages)}, ensure_ascii=False))
        # details.append(json.dumps({"name": "query_chunk", "query": query}, ensure_ascii=False))
    return response_str, details


async def query_without_auth(data):
    try:
        index_name = indexname_prefix_relation
        business = indexname_prefix_relation
        # 当openid从传入是异常或者没有的时候，就用标准的Chunk数据
        if "openid" in data.keys() and data["openid"].strip() != "":
            try:
                kol_id = await get_kol_id(data["openid"])
                if kol_id != "":
                    if is_test:
                        index_name = index_name + "_" + kol_id + "_test"
                    else:
                        index_name = index_name + "_" + kol_id
                    business = business + "_" + kol_id
                    openid = data["openid"]
                else:
                    openid = default_openid
                    kol_id = openid
            except Exception:
                logger.error(traceback.format_exc())
                response = {
                    'status': 500,
                    'message': 'Request InternetId by openId error!'
                }
                return web.json_response(status=500, data=response)
        else:
            openid = default_openid
            kol_id = openid

        similarity_cutoff = data["similarity_cutoff"] if "similarity_cutoff" in data.keys() else 0.6
        similarity_top_k = data["similarity_top_k"] if "similarity_top_k" in data.keys() else 3
        is_rerank = data["is_rerank"] if "is_rerank" in data.keys() else False
        metadata = data["metadata"] if "metadata" in data.keys() else {"query_type": "QA"}
        keywords_filter = data["keywords_filter"] if "keywords_filter" in data.keys() else is_keywords_filter
        if "messages" in data.keys():
            messages = data["messages"]
        else:
            response = {
                'status': 400,
                'data': {}
            }
            response = json.loads(json.dumps(response, ensure_ascii=False))
            return web.json_response(status=400, data=response)

        query = []

        last_timestamp = None
        length = len(messages)
        for i in range(len(messages)):
            if messages[length - i - 1]["role"] == "user":
                if "additional_kwargs" in messages[length - i - 1].keys():
                    ms_timestamp = messages[length - i - 1]["additional_kwargs"]["timestamp"]
                    if last_timestamp is None or last_timestamp - ms_timestamp <= 3600:
                        if messages[length - i - 1]["additional_kwargs"]["type"] == "audio":
                            query.append(
                                messages[length - i - 1]["additional_kwargs"]["audio_text"].replace("/bye", ""))
                        elif messages[length - i - 1]["additional_kwargs"]["type"] == "text":
                            query.append(messages[length - i - 1]["content"].replace("/bye", ""))
                        last_timestamp = ms_timestamp
                else:
                    query.append(messages[length - i - 1]["content"].replace("/bye", ""))
            if len(query) >= query_length:
                break

        logger.info('data query: {}'.format(data))
        try:
            # details = [
            #     json.dumps({"messages": [m["role"] + ":" + m["additional_kwargs"]["dj_mess_id"] for m in messages]},
            #                ensure_ascii=False)]
            details = [json.dumps({"messages": [
                [f'dj_mess_id:{m["additional_kwargs"]["dj_mess_id"]}', f'{m["role"]}: {m["content"]}'] for m in
                messages]},
                ensure_ascii=False)]
        except Exception:
            # details = [json.dumps({"messages": [
            #     [f'dj_mess_id:{m["additional_kwargs"]["dj_mess_id"]}', f'{m["role"]}: {m["content"]}'] for m in
            #     messages]},
            #     ensure_ascii=False)]
            details = [json.dumps({"messages": [m for m in messages]},
                                  ensure_ascii=False)]
        if is_divided_query:
            result, details = await request_faq(query[0], details, index_name, metadata, is_rerank, keywords_filter,
                                                similarity_cutoff, similarity_top_k)
            for i in range(1, min(3, len(query))):
                # if result["score"] != 0:
                if not len(result) == 0:
                    break
                result, details = await request_faq(query[i], details, index_name, metadata, is_rerank, keywords_filter,
                                                    similarity_cutoff, similarity_top_k)
            # if result["score"] == 0 and len(query) > 1:
            if len(result) == 0 and len(query) > 1:
                metadata["query_type"] = "QA"
                details.append(json.dumps({"name": "retry", "result": "single messages missed!"}, ensure_ascii=False))
                result, details = await request_faq(",".join(query), details, index_name, metadata, is_rerank,
                                                    keywords_filter,
                                                    similarity_cutoff, similarity_top_k)
        else:
            result, details = await request_faq(",".join(query), details, index_name, metadata, is_rerank,
                                                keywords_filter,
                                                similarity_cutoff,
                                                similarity_top_k)
        # result["detail"] = details
        result_response = {"docs": result, "detail": details}
        logger.info('【data query result】: {}'.format(result_response))
        response = {
            'status': 200,
            'data': {"docs": result, "detail": []}
        }
        response = json.loads(json.dumps(response, ensure_ascii=False))
        return web.json_response(status=200, data=response)
    except Exception:
        logger.error(traceback.format_exc())
        response = {
            'status': 500,
            'data': 'Interval Error'
        }
        return web.json_response(status=500, data=response)


async def QueryRelationRequest(request):
    try:
        data = await request.json()
    except json.JSONDecodeError:
        return web.Response(text="Invalid JSON", status=400)
    try:
        return await query_without_auth(data)
    except Exception:
        logger.error(traceback.format_exc())
        response = {
            'status': 500,
            'message': traceback.format_exc()
        }
        return web.json_response(status=500, data=response)
