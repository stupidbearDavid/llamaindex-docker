#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import traceback

import time
from aiohttp import web

from config import db_type, is_keywords_filter, rec_similarity_cutoff, is_smooth_score, is_divided_query, query_length, \
    default_openid, is_test, indexname_prefix_item
from service.logger import logger
from service.model import Singleton
from service.gpt_request import request_deepseek_pure
from service.indcing import switch_index, index_query
from service.keywords_filter import keyword_filter
from service.neo4j_index import score_smooth
from service.prompt_utils import create_prompt_rec_check, create_prompt_message_check
from service.webservice.llamaindex.utils import get_kol_id


def check_is_rec(sign):
    return sign.strip().lower() == 'false'


def check_is_contain_content(sign):
    if sign.strip().lower() not in ["false", "true"]:
        return False
    else:
        return sign.strip().lower() == 'true'


async def request_is_reply(messages):
    formated_messages = [{"role": m["role"], "content": m["additional_kwargs"]["audio_text"] if m["additional_kwargs"][
                                                                                                    "type"] == "audio" else
    m["content"]} for m in messages]
    # messages_str = ""
    # i = 0
    # for m in formated_messages[-query_length:]:
    #     if m["role"] == "user":
    #         messages_str += "[" + m["role"] + "]" + ":" + m["content"] + "\n"
    #         i += 1
    #     if i >= query_length:
    #         break

    messages_str = "\n".join(["[" + m["role"] + "]" + ":" + m["content"] for m in formated_messages[-query_length:]])
    prompt = create_prompt_rec_check(messages_str)
    sign = await request_deepseek_pure(model="deepseek-coder", messages=prompt)
    logger.info("[deepseek response] {}".format(sign))
    return sign


async def request_single_message(message):
    prompt = create_prompt_message_check(message)
    sign = await request_deepseek_pure(model="deepseek-coder", messages=prompt)
    logger.info("[deepseek single response] {}".format(sign))
    return check_is_contain_content(sign)


def rerank_response(query, answers, similarity_cutoff):
    rerank_results = Singleton.instance().rerank(query, answers)
    # rerank_results = indexStore.reranker_model.rerank(query, answers)
    filtered_scores = [x for x in rerank_results["rerank_scores"] if x >= similarity_cutoff]
    result = []
    for i in range(len(filtered_scores)):
        result.append([rerank_results["rerank_passages"][i], rerank_results["rerank_scores"][i]])
    return result


async def request_rerank(query_ori, details, index_name, metadata, is_rerank, keywords_filter, similarity_cutoff,
                         similarity_top_k,
                         rec_similarity_cutoff_=rec_similarity_cutoff):
    INDEX = switch_index(index_name)
    if keywords_filter:
        query = keyword_filter(query_ori)
    else:
        query = query_ori

    if is_rerank:
        passages_sim = await index_query(INDEX, query, 20, similarity_cutoff, metadata)

        passages = {}
        if db_type == "redis":
            for p in range(len(passages_sim)):
                score = score_smooth(score=passages_sim[p][1]) if is_smooth_score else passages_sim[p][1]
                if score >= similarity_cutoff:
                    passages[passages_sim[p][0]] = passages_sim[p]
                    passages[passages_sim[p][0]][1] = score
        else:
            for p in passages_sim:
                score = score_smooth(score=passages_sim[p][1]) if is_smooth_score else passages_sim[p][1]
                if score >= similarity_cutoff:
                    passages[p] = passages_sim[p]
                    passages[p][1] = score

        details.append(json.dumps({"name": "query_chunk", "query": query, "result": str([[passages[k][1],
                                                                                          passages[k][2]["unique_id"]]
                                                                                         for k in
                                                                                         passages.keys()])},
                                  ensure_ascii=False))
        # details.append(json.dumps({"name": "query_chunk", "query": query},
        #                           ensure_ascii=False))
        response = rerank_response(query, passages.keys(), rec_similarity_cutoff_)[
                   :similarity_top_k]
        if db_type == "redis":
            response_str = []
            rec_items = []
            for k in response:
                if passages[k[0]][2]["unique_id"] not in rec_items:
                    response_str.append([passages[k[0]][2]["unique_id"], k[1], rec_similarity_cutoff_])
                    rec_items.append(passages[k[0]][2]["unique_id"])
        else:
            response_str = []
            for k in response:
                response_str.append([passages[k[0]][0], k[1], rec_similarity_cutoff_])
        details.append(json.dumps({"name": "reranked", "result": str(response_str)}, ensure_ascii=False))
        # details.append(json.dumps({"name": "reranked"}, ensure_ascii=False))
    else:
        passages_sim = await index_query(INDEX, query, similarity_top_k,
                                         rec_similarity_cutoff_, metadata)
        passages = {}
        for p in range(len(passages_sim)):
            score = passages_sim[p][1]#score_smooth(score=passages_sim[p][1]) if is_smooth_score else passages_sim[p][1]
            if score >= rec_similarity_cutoff_:
                passages[p] = passages_sim[p]
                passages[p][1] = score
        if db_type == "redis":
            response_str = []
            rec_items = []
            for k in passages.keys():
                if passages[k][0] not in rec_items:
                    response_str.append([passages[k][2]['unique_id'], passages[k][1], rec_similarity_cutoff_])
                    rec_items.append(passages[k][2]['unique_id'])
        else:
            response_str = []
            for k in passages.keys():
                response_str.append([passages[k][2]['unique_id'], passages[k][1], rec_similarity_cutoff_])
        details.append(json.dumps({"name": "reranked", "result": str(response_str)}, ensure_ascii=False))
        # details.append(json.dumps({"name": "reranked"}, ensure_ascii=False))
    result = {"items": response_str}
    result["detail"] = details
    return result, details


async def rec_stratege_from_message(no_need_rec, messages, index_name, similarity_cutoff, similarity_top_k, is_rerank,
                                    keywords_filter):
    query = []
    last_timestamp = None
    length = len(messages)
    for i in range(len(messages)):
        if messages[length - i - 1]["role"] == "user":
            if "additional_kwargs" in messages[length - i - 1].keys():
                ms_timestamp = messages[length - i - 1]["additional_kwargs"]["timestamp"]
                if last_timestamp is None or last_timestamp - ms_timestamp <= 3600:
                    if messages[length - i - 1]["additional_kwargs"]["type"] == "audio":
                        query.append(messages[length - i - 1]["additional_kwargs"]["audio_text"].replace("/bye", ""))
                    elif messages[length - i - 1]["additional_kwargs"]["type"] == "text":
                        query.append(messages[length - i - 1]["content"].replace("/bye", ""))
                    last_timestamp = ms_timestamp
            else:
                query.append(messages[length - i - 1]["content"].replace("/bye", ""))
        if len(query) >= query_length:
            break

    try:
        details = [json.dumps({"messages": [
                [f'dj_mess_id:{m["additional_kwargs"]["dj_mess_id"]}', f'{m["role"]}: {m["content"]}'] for m in
                messages]},
                              ensure_ascii=False)]
        # details = [json.dumps({"messages": [m for m in query]},
        #                       ensure_ascii=False)]
    except Exception:
        # details = [json.dumps({"messages": [
        #         [f'dj_mess_id:{m["additional_kwargs"]["dj_mess_id"]}', f'{m["role"]}: {m["content"]}'] for m in
        #         messages]},
        #                       ensure_ascii=False)]
        details = [json.dumps({"messages": [m for m in query]},
                              ensure_ascii=False)]
    if len(query) > 0:
        try:
            sign = await request_single_message(query[0])
        except Exception:
            logger.error("[deepseek error] {}".format(traceback.format_exc()))
            sign = True

        # 首句包含症状描述和推荐内容
        if sign is True:
            result, details = await request_rerank(query[0], details, index_name, {}, is_rerank, keywords_filter,
                                                   similarity_cutoff,
                                                   similarity_top_k, rec_similarity_cutoff_=rec_similarity_cutoff)
        else:
            details.append(json.dumps({"result": "single message contains no content"}, ensure_ascii=False))
            result = {"items": [], "detail": details}
    else:
        details.append(json.dumps({"result": "no query"}, ensure_ascii=False))
        result = {"items": [], "detail": details}
    # 如果总结的query没有推荐出商品
    if len(result["items"]) == 0:
        if no_need_rec == "":
            details.append(json.dumps({"result": "no gpt query"}, ensure_ascii=False))
        else:
            # LLM总结想要了解的问题
            result, details = await request_rerank(no_need_rec, details, index_name, {}, is_rerank, keywords_filter,
                                                   similarity_cutoff,
                                                   similarity_top_k, rec_similarity_cutoff_=rec_similarity_cutoff)
    # 如果总结的query没有推荐出商品
    if len(result["items"]) == 0:
        details.append(json.dumps({"name": "gpt request", "query": no_need_rec, "result": "gpt messages missed!"},
                                  ensure_ascii=False))
        if is_divided_query:
            for i in range(1, min(3, len(query))):
                if len(result["items"]) > 0:
                    break
                result, details = await request_rerank(query[i], details, index_name, {}, is_rerank, keywords_filter,
                                                       similarity_cutoff,
                                                       similarity_top_k)
            if len(result["items"]) == 0 and len(query) > 1:
                details.append(json.dumps({"name": "multi_query", "query": query, "result": "single messages missed!"},
                                          ensure_ascii=False))
                result, details = await request_rerank(",".join(query), details, index_name, {}, is_rerank,
                                                       keywords_filter,
                                                       similarity_cutoff, similarity_top_k)
        else:
            result, details = await request_rerank(",".join(query), details, index_name, {}, is_rerank, keywords_filter,
                                                   similarity_cutoff,
                                                   similarity_top_k)
    return result, details


async def query_without_auth(data):
    try:
        index_name = indexname_prefix_item
        business = indexname_prefix_item
        # 当openid从传入是异常或者没有的时候，就用标准的Chunk数据
        if "openid" in data.keys() and data["openid"].strip() != "":
            try:
                kol_id = await get_kol_id(data["openid"])
                if kol_id != "":
                    if is_test:
                        index_name = index_name + "_" + kol_id + "_test"
                    else:
                        index_name = index_name + "_" + kol_id
                    business = business + "_" + kol_id
                    openid = data["openid"]
                else:
                    openid = default_openid
                    kol_id = openid
            except Exception:
                logger.error(traceback.format_exc())
                response = {
                    'status': 500,
                    'message': 'Request InternetId by openId error!'
                }
                return web.json_response(status=500, data=response)
        else:
            openid = default_openid
            kol_id = openid
        similarity_cutoff = data["similarity_cutoff"] if "similarity_cutoff" in data.keys() else 0.0
        similarity_top_k = data["similarity_top_k"] if "similarity_top_k" in data.keys() else 20
        is_rerank = data["is_rerank"] if "is_rerank" in data.keys() else True
        keywords_filter = data["keywords_filter"] if "keywords_filter" in data.keys() else is_keywords_filter

        logger.info('【recommedation query】: {}'.format(data))
        if "messages" in data.keys():
            messages = data["messages"]
        else:
            response = {
                'status': 400,
                'data': {}
            }
            response = json.loads(json.dumps(response, ensure_ascii=False))
            return web.json_response(status=400, data=response)

        try:
            gpt_query = await request_is_reply(messages)
        except Exception:
            logger.error("[deepseek error] {}".format(traceback.format_exc()))
            gpt_query = ""

        if check_is_rec(gpt_query) is True:
            result = {"items": [], "detail": [json.dumps({"result": "No need to recommend!"}, ensure_ascii=False)]}
        else:
            # tokens = gpt_query.strip().split("：")
            # gpt_query = tokens[1] if len(tokens) > 1 else gpt_query
            result, details = await rec_stratege_from_message(gpt_query, messages, index_name,
                                                              similarity_cutoff, similarity_top_k, is_rerank,
                                                              keywords_filter)

        logger.info('【data query result】: {}'.format(result))
        result["detail"] = []
        response = {
            'status': 200,
            'data': result
        }
        response = json.loads(json.dumps(response, ensure_ascii=False))
        return web.json_response(status=200, data=response)
    except Exception:
        logger.error(traceback.format_exc())
        response = {
            'status': 500,
            'data': 'Interval Error'
        }
        return web.json_response(status=500, data=response)


async def QueryItemRequest(request):
    try:
        data = await request.json()
    except json.JSONDecodeError:
        return web.Response(text="Invalid JSON", status=400)
    try:
        return await query_without_auth(data)
    except Exception:
        logger.error(traceback.format_exc())
        response = {
            'status': 500,
            'message': traceback.format_exc()
        }
        return web.json_response(status=500, data=response)
