#!/usr/bin/env python
# -*- coding: utf-8 -*-

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import os


root_path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + os.path.sep + ".") + '/'


class CacheFileHandler(tornado.web.RequestHandler):

    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
        self.set_header("Access-Control-Allow-Methods", "GET, POST, OPTIONS")

    def options(self):
        self.set_status(204)
        self.finish()

    def get(self, filename):

        file_path = os.path.join(os.path.join(root_path + '../uploads', filename))
        if os.path.exists(file_path):
            self.set_header('Content-Type', 'image/png')
            with open(file_path, 'rb') as file:
                self.write(file.read())

        else:
            self.set_status(404)
            self.finish("File not found")


if __name__ == "__main__":
    from tornado.options import define, options

    define("port", default=8888, help="run on the given port", type=int)
    tornado.options.parse_command_line()
    app = tornado.web.Application(handlers=[(r"/", CacheFileHandler)])
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
