#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
import traceback

from aiohttp import web

from service.logger import logger
from service.indcing import add_relation_index
from service.authority import check_authority


"""
request structure:
{
    "from_name": "胡子哥",
    "from_r_to": "儿子",
    "to_name": "苏科勤",
    "to_r_from": "父亲"
}
"""
async def AddCharacterRelationRequest(request):
    try:
        data = await request.json()
    except json.JSONDecodeError:
        return web.Response(text="Invalid JSON", status=400)
    try:
        from_name = data['from_name']
        from_r_to = data['from_r_to'] if "from_r_to" in data.keys() else None
        to_name = data['to_name']
        to_r_from = data['to_r_from'] if "to_r_from" in data.keys() else None

        logger.info("add character relationship: {}".format(data))
        if check_authority(data, 'pushfile'):

            if from_r_to is not None:
                # from_label, from_props, from_key, to_label, to_props, to_key, relationship
                add_relation_index(from_label="人物", from_props={"name": from_name},
                                         from_key="name", to_label="人物",
                                         to_props={"name": to_name}, to_key="name",
                                         relationship=from_r_to)

            if to_r_from is not None:
                add_relation_index(from_label="人物", from_props={"name": to_name},
                                         from_key="name", to_label="人物",
                                         to_props={"name": from_name}, to_key="name",
                                         relationship=to_r_from)
            response = {
                'status': 200,
                'message': "ok"
            }
            response = json.loads(json.dumps(response, ensure_ascii=False))
            return web.json_response(status=200, data=response)
        else:
            response = {
                'status': 401,
                'message': "Authority deny!"
            }
            return web.json_response(status=401, data=response)

    except Exception:
        logger.info(traceback.format_exc())
        response = {
            'status': 500,
            'message': 'Interval Error!'
        }
        return web.json_response(status=500, data=response)
