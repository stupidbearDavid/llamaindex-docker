#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
import traceback

from aiohttp import web

from config import db_type, is_divided_query, is_keywords_filter, is_smooth_score, query_length, default_openid, \
    is_test, indexname_prefix_knowledge
from service.keywords_filter import keyword_filter
from service.logger import logger
from service.model import Singleton

root_path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + os.path.sep + ".") + '/'


async def QueryRelatedRequest(request):
    try:
        data = await request.json()
    except json.JSONDecodeError:
        return web.Response(text="Invalid JSON", status=400)
    try:

        query = data["query"]
        passages = data["passages"]
        response = Singleton.instance().rerank(query, passages)
        return web.json_response(data=response, status=200)
    except Exception:
        logger.error(traceback.format_exc())
        response = {
            'status': 500,
            'message': traceback.format_exc()
        }
        return web.json_response(status=500, data=response)
