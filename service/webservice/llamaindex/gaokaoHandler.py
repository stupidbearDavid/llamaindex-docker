#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
import traceback

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

from service.authority import get_email, is_authorized, get_email_oauth
from service.gpt_request import request_gpt
from service.mysql_utils import query_school_content

root_path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + os.path.sep + ".") + '/'


class GaokaoHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
        self.set_header("Access-Control-Allow-Methods", "GET, POST, OPTIONS")

    def options(self):
        self.set_status(204)
        self.finish()

    def post(self):
        try:
            data = json.loads(self.request.body)
            if 'token' in data.keys() or 'oauthToken' in data.keys():
                accesstoken = {'access-session': data['token']} if 'token' in data.keys()\
                    else {'oauth-token': data['oauthToken']}
                accesstoken["Content-Type"] = "application/json"

                token = data['token'] if 'token' in data.keys() else data['oauthToken']
                email = get_email(token) if 'token' in data.keys() else get_email_oauth(token)
                index_name = data["indexname"]

                if is_authorized(index_name, email, 'query'):
                    school_name = request_gpt(headers=accesstoken, data=[
                        {
                            'role': "user",
                            'content': "帮我把我的问题中的学校名称提取出来，只回复我学校的名称即可，如果是简写名称，回复我全称，"
                                       "例如：可以介绍一下华中科大的软件技术专业吗？回复我'华中科技大学'即可."
                                       "如果没有明确的学校名称，则回复我'无'"
                                       "我的问题是："
                        },
                        {
                            'role': "user",
                            'content': data["query"]
                        }
                    ])
                    if school_name == '无':
                        self.write("")
                    else:
                        print(school_name)
                        if "similarity_cutoff" in data.keys():
                            similarity_cutoff = data["similarity_cutoff"]
                        else:
                            similarity_cutoff = 0.9
                        if "similarity_top_k" in data.keys():
                            similarity_top_k = data["similarity_top_k"]
                        else:
                            similarity_top_k = 3
                        answer = query_school_content(school_name, data["query"], similarity_cutoff, similarity_top_k)
                        self.write(answer)
                else:
                    response = {
                        'status': 401,
                        'message': "Authority deny!"
                    }
                    self.set_status(401)
                    self.write(response)
            else:
                response = {
                    'status': 401,
                    'message': "Authority deny!"
                }
                self.set_status(401)
                self.write(response)
        except Exception:
            print(traceback.format_exc())
            response = {
                'status': 500,
                'message': traceback.format_exc()
            }
            self.set_status(500)
            return self.write(response)


if __name__ == "__main__":
    from tornado.options import define, options

    define("port", default=8888, help="run on the given port", type=int)
    tornado.options.parse_command_line()
    app = tornado.web.Application(handlers=[(r"/", GaokaoHandler)])
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
