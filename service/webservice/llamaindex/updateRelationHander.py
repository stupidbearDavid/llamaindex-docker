#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
import traceback

from aiohttp import web

from service.indcing import add_index, add_QA_index, delete_index
from service.redis_index import delete_doc_by_similiarity
from service.webservice.llamaindex.utils import get_request, get_kol_id
from service.logger import logger
from config import relation_location, root_path, db_type, default_openid, is_test, indexname_prefix_relation
from service.file_utils import save_file, load_file


def check_data(old_data, data):
    old_q = {}
    for d in old_data:
        if d["id"] not in old_q.keys():
            old_q[d["id"]] = [d["nickname"], d["relation"]]

    need_upload = []
    need_delete = []
    check_q = []
    # need upload
    for qa in data:
        # 去重
        if qa["id"] not in check_q:
            check_q.append(qa["id"])
        else:
            continue
        if qa["id"] not in old_q.keys():
            need_upload.append(qa)
        # 如果问题内容已经存在
        else:
            if qa["relation"] != old_q[qa["id"]][1] or qa["nickname"] != old_q[qa["id"]][0]:
                # print(qa["answer"], old_q[qa["question"]][0], qa["threshold"], old_q[qa["question"]][1])
                need_upload.append({"nickname": qa["nickname"], "relation": qa["relation"]})
    # need delete
    for d in old_data:
        if d["id"] not in check_q:
            need_delete.append({"nickname": d["nickname"], "relation": d["relation"]})
    return need_upload, need_delete


async def PushRelationDataRequest(request):
    try:
        data = await request.json()
    except json.JSONDecodeError:
        return web.Response(text="Invalid JSON", status=400)
    try:
        logger.info("update Relation data: {}".format(data))
        indexname = indexname_prefix_relation
        business = indexname_prefix_relation
        # 当openid从传入是异常或者没有的时候，就用标准的Chunk数据
        # if "openid" in data.keys() and data["openid"].strip() != "":
        #     try:
        #         kol_id = await get_kol_id(data["domainid"])
        #         if kol_id != "":
        #             indexname = indexname + "_" + kol_id
        #             business = business + "_" + kol_id
        #             openid = data["openid"]
        #         else:
        #             openid = default_openid
        #             kol_id = openid
        #     except Exception:
        #         logger.error(traceback.format_exc())
        #         response = {
        #             'status': 500,
        #             'message': 'Request InternetId by openId error!'
        #         }
        #         return web.json_response(status=500, data=response)
        # else:
        #     openid = default_openid
        #     kol_id = openid

        if not "domainid" in data.keys() or str(data["domainid"]).strip() == "":
            response = {
                'status': 402,
                'message': "parameter 'domainid' is missed!"
            }
            response = json.loads(json.dumps(response, ensure_ascii=False))
            return web.json_response(status=402, data=response)
        domainid = str(data["domainid"]).strip()
        if is_test:
            indexname = indexname + "_" + domainid + "_test"
            business = business + "_" + domainid + "_test"
        else:
            indexname = indexname + "_" + domainid
            business = business + "_" + domainid

        is_force_update = data["is_force_update"] if "is_force_update" in data.keys() else False

        # openid = "93D503DCB466930ABE8B93F82B5DB960"
        json_data = await get_request(relation_location + "?domainid=" + domainid)

        # new_data = json_data["data"][domainid] if domainid in json_data["data"].keys() else []
        new_data = json_data["data"]

        # 如果当前的openid不存在，那么就添加一个文件夹目录
        if not os.path.exists(os.path.join(root_path + 'service/uploads', domainid)):
            os.mkdir(os.path.join(root_path + 'service/uploads', domainid))

        if os.path.exists(
                os.path.join(root_path + 'service/uploads', domainid, "saved_relation_json.json")):
            old_data = load_file(
                os.path.join(root_path + 'service/uploads', domainid, "saved_relation_json.json"))
            if is_force_update:
                await delete_index(indexname)
                logger.warning("Delete {}!".format(indexname))
                need_upload, need_delete = check_data([], new_data)
            else:
                need_upload, need_delete = check_data(old_data["data"], new_data)
        else:
            need_upload, need_delete = check_data([], new_data)

        logger.info("update faq: {}".format(len(need_upload)))
        logger.info("delete faq: {}".format(len(need_delete)))
        save_file(json.dumps(json_data, ensure_ascii=False),
                  os.path.join(root_path + 'service/uploads', domainid,
                               "saved_relation_json.json"))

        i = 0
        for d in need_upload:
            if db_type == "redis":
                response_str = await add_QA_index(indexname, d["nickname"],
                                                  f'用户的昵称：{d["nickname"]}, 关系信息： {d["relation"]}', "saved_relation_json.json",
                                                  None)
            else:
                response_str = await add_QA_index(indexname, {"unique_id": domainid + "_" + str(i) + "主播",
                                                              "text": d["question"], "openid": domainid,
                                                              "relation": d["relation"], "business": business},
                                                  {"unique_id": domainid + "_" + str(i) + "粉丝",
                                                   "text": d["answer"],
                                                   "openid": domainid, "business": business}, None)
            i += 1
        for d in need_delete:
            if db_type == "redis":
                response_str = delete_doc_by_similiarity(indexname, d["nickname"])

        response = {
            'status': 200,
            'message': "ok"
        }
        response = json.loads(json.dumps(response, ensure_ascii=False))
        return web.json_response(status=200, data=response)

    except Exception:
        logger.error(traceback.format_exc())
        response = {
            'status': 500,
            'message': 'Interval Error!'
        }
        return web.json_response(status=500, data=response)
