#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import json
import traceback

from aiohttp import web

from service.authority import get_email, get_authorize_level, authority_level, delete_index_regex

root_path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + os.path.sep + ".") + '/'


async def DeleteAuthRequest(request):
    try:
        data = await request.json()
    except json.JSONDecodeError:
        return web.Response(text="Invalid JSON", status=400)
    try:
        index_name = data['indexname']
        regexes = data['pattern']
        if 'token' in data.keys():
            accesstoken = data['token']
            email = await get_email(accesstoken)
            p = get_authorize_level(index_name, email)
            mp = get_authorize_level("llamaindex_managers", email)
            if ((mp >= authority_level["manager"] or p >= authority_level[
                "manager"]) and not index_name == "llamaindex_managers") or \
                    p == authority_level["owner"]:
                delete_index_regex(index_name, regexes)
                response = {
                    'status': 200,
                    'message': f"Regexes is deleted from {index_name} successfully."
                }
                response = json.loads(json.dumps(response, ensure_ascii=False))
                return web.json_response(status=200, data=response)
            else:
                response = {
                    'status': 401,
                    'message': "Authority deny!"
                }
                return web.json_response(status=401, data=response)
        else:
            response = {
                'status': 401,
                'message': "Authority deny!"
            }
            return web.json_response(status=401, data=response)
    except Exception:
        response = {
            'status': 500,
            'message': traceback.format_exc()
        }
        return web.json_response(status=500, data=response)