#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
import traceback

from aiohttp import web

from service.authority import get_email, get_authoritylist, get_authority_level, authority_level

root_path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + os.path.sep + ".") + '/'


async def AuthorityEditRequest(request):
    try:
        data = await request.json()
    except json.JSONDecodeError:
        return web.Response(text="Invalid JSON", status=400)
    try:
        if 'token' in data.keys():
            accesstoken = data['token']
            email = await get_email(accesstoken)
            resdata = {}
            indexlists = get_authoritylist()
            mp = get_authority_level(indexlists["llamaindex_managers"], email)
            for indexname in indexlists.keys():
                p = get_authority_level(indexlists[indexname], email)
                if ((mp >= authority_level["manager"] or p >= authority_level[
                    "manager"]) and not indexname == "llamaindex_managers") or \
                        p == authority_level["owner"]:
                    resdata[indexname] = indexlists[indexname]
            response_str = json.loads(json.dumps(resdata, ensure_ascii=False))
            return web.json_response(status=200, data=response_str)
        else:
            response = {
                'status': 401,
                'message': "Authority deny!"
            }
            return web.json_response(status=401, data=response)
    except Exception:
        print(traceback.format_exc())
        response = {
            'status': 500,
            'message': traceback.format_exc()
        }
        return web.json_response(status=500, data=response)
