#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
import traceback

from aiohttp import web

from service.indcing import add_index, add_QA_index, delete_index
from service.redis_index import delete_doc_by_similiarity
from service.webservice.llamaindex.utils import get_request, get_kol_id
from service.logger import logger
from config import faq_location, root_path, db_type, default_openid, is_test, indexname_prefix_faq
from service.file_utils import save_file, load_file


def check_data(old_data, data):
    old_q = {}
    for d in old_data:
        if d["question"] not in old_q.keys():
            old_q[d["question"]] = [d["answer"], d["threshold"]]

    need_upload = []
    need_delete = []
    check_q = []
    # need upload
    for qa in data:
        # 去重
        if qa["question"] not in check_q:
            check_q.append(qa["question"])
        else:
            continue
        if qa["question"] not in old_q.keys():
            need_upload.append(qa)
        # 如果问题内容已经存在
        else:
            if qa["answer"] != old_q[qa["question"]][0] or qa["threshold"] != old_q[qa["question"]][1]:
                # print(qa["answer"], old_q[qa["question"]][0], qa["threshold"], old_q[qa["question"]][1])
                need_upload.append({"question": qa["question"], "answer": qa["answer"], "threshold": qa["threshold"]})
    # need delete
    for d in old_data:
        if d["question"] not in check_q:
            need_delete.append({"question": d["question"], "answer": d["answer"], "threshold": d["threshold"]})
    return need_upload, need_delete


async def PushDataRequest(request):
    try:
        data = await request.json()
    except json.JSONDecodeError:
        return web.Response(text="Invalid JSON", status=400)
    try:
        logger.info("update FAQ data: {}".format(data))
        indexname = indexname_prefix_faq
        business = indexname_prefix_faq
        # 当openid从传入是异常或者没有的时候，就用标准的Chunk数据
        # if "openid" in data.keys() and data["openid"].strip() != "":
        #     try:
        #         kol_id = await get_kol_id(data["domainid"])
        #         if kol_id != "":
        #             indexname = indexname + "_" + kol_id
        #             business = business + "_" + kol_id
        #             openid = data["openid"]
        #         else:
        #             openid = default_openid
        #             kol_id = openid
        #     except Exception:
        #         logger.error(traceback.format_exc())
        #         response = {
        #             'status': 500,
        #             'message': 'Request InternetId by openId error!'
        #         }
        #         return web.json_response(status=500, data=response)
        # else:
        #     openid = default_openid
        #     kol_id = openid

        if not "domainid" in data.keys() or str(data["domainid"]).strip() == "":
            response = {
                'status': 402,
                'message': "parameter 'domainid' is missed!"
            }
            response = json.loads(json.dumps(response, ensure_ascii=False))
            return web.json_response(status=402, data=response)
        domainid = str(data["domainid"]).strip()
        if is_test:
            indexname = indexname + "_" + domainid + "_test"
            business = business + "_" + domainid + "_test"
        else:
            indexname = indexname + "_" + domainid
            business = business + "_" + domainid

        is_force_update = data["is_force_update"] if "is_force_update" in data.keys() else False

        # openid = "93D503DCB466930ABE8B93F82B5DB960"
        json_data = await get_request(faq_location + "?domainid=" + domainid)

        # new_data = json_data["data"][domainid] if domainid in json_data["data"].keys() else []
        new_data = json_data["data"][domainid]

        # 如果当前的openid不存在，那么就添加一个文件夹目录
        if not os.path.exists(os.path.join(root_path + 'service/uploads', domainid)):
            os.mkdir(os.path.join(root_path + 'service/uploads', domainid))

        if os.path.exists(
                os.path.join(root_path + 'service/uploads', domainid, "saved_faq_json.json")):
            old_data = load_file(
                os.path.join(root_path + 'service/uploads', domainid, "saved_faq_json.json"))
            if is_force_update:
                await delete_index(indexname)
                logger.warning("Delete {}!".format(indexname))
                need_upload, need_delete = check_data([], new_data)
            else:
                need_upload, need_delete = check_data(old_data["data"][domainid], new_data)
        else:
            need_upload, need_delete = check_data([], new_data)

        logger.info("update faq: {}".format(len(need_upload)))
        logger.info("delete faq: {}".format(len(need_delete)))
        save_file(json.dumps(json_data, ensure_ascii=False),
                  os.path.join(root_path + 'service/uploads', domainid,
                               "saved_faq_json.json"))

        i = 0
        for d in need_upload:
            if db_type == "redis":
                response_str = await add_QA_index(indexname, d["question"],
                                                  d["answer"], "saved_faq_json.json", {"threshold": d["threshold"]})
            else:
                response_str = await add_QA_index(indexname, {"unique_id": domainid + "_" + str(i) + "Questions",
                                                              "text": d["question"], "openid": domainid,
                                                              "threshold": d["threshold"], "business": business},
                                                  {"unique_id": domainid + "_" + str(i) + "Answers", "text": d["answer"],
                                                   "openid": domainid, "business": business}, None)
            i += 1
        for d in need_delete:
            if db_type == "redis":
                response_str = delete_doc_by_similiarity(indexname, d["question"])

        response = {
            'status': 200,
            'message': "ok"
        }
        response = json.loads(json.dumps(response, ensure_ascii=False))
        return web.json_response(status=200, data=response)

    except Exception:
        logger.error(traceback.format_exc())
        response = {
            'status': 500,
            'message': 'Interval Error!'
        }
        return web.json_response(status=500, data=response)
