#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
import traceback

from aiohttp import web

from service.logger import logger
from service.indcing import get_indexlist
from service.authority import get_email, get_email_oauth, get_authorize_level, authority_level

root_path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + os.path.sep + ".") + '/'


async def IndexListRequest(request):
    try:
        data = await request.json()
    except json.JSONDecodeError:
        return web.Response(text="Invalid JSON", status=400)
    try:
        if 'token' in data.keys() or 'oauthToken' in data.keys():
            accesstoken = data['token'] if 'token' in data.keys() else data['oauthToken']
            email = await get_email(accesstoken) if 'token' in data.keys() else await get_email_oauth(accesstoken)
            indexlists = get_indexlist()
            if get_authorize_level("llamaindex_managers", email) >= authority_level['owner']:
                resdata = {"su_apilist": []}
                for key in indexlists.keys():
                    resdata["su_apilist"].append(key)
                response_str = json.loads(json.dumps(resdata, ensure_ascii=False))
                return web.json_response(status=200, data=response_str)
            else:
                resdata = {"apilist": []}
                for indexname in indexlists.keys():
                    if get_authorize_level(indexname, email) >= authority_level['query']:
                        resdata["apilist"].append(indexname)
                response_str = json.loads(json.dumps(resdata, ensure_ascii=False))
                return web.json_response(status=200, data=response_str)
        else:
            response = {
                'status': 401,
                'message': "Authority deny!"
            }
            return web.json_response(status=401, data=response)
    except Exception:
        logger.error(traceback.format_exc())
        response = {
            'status': 500,
            'message': traceback.format_exc()
        }
        return web.json_response(status=500, data=response)