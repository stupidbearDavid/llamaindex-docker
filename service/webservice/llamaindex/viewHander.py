#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import json
import traceback

from aiohttp import web

from service.logger import logger
from service.indcing import get_doc
from service.authority import check_authority

root_path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + os.path.sep + ".") + '/'


async def ViewRequest(request):
    try:
        data = await request.json()
    except json.JSONDecodeError:
        return web.Response(text="Invalid JSON", status=400)
    try:
        index_name = data['indexname']
        file_name = data['filename']

        if check_authority(data, 'pushfile'):
            response = get_doc(index_name, file_name)
            # 返回生成的 HTML 页面
            response = json.loads(json.dumps(response, ensure_ascii=False))
            return web.json_response(status=200, data=response)
        else:
            response = {
                'status': 401,
                'message': "Authority deny!"
            }
            return web.json_response(status=401, data=response)

    except Exception:
        logger.info(traceback.format_exc())
        response = {
            'status': 500,
            'message': 'Interval Error!'
        }
        return web.json_response(status=500, data=response)