#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import json
import traceback

import time
from aiohttp import web

from config import is_keywords_filter, default_openid, is_test, indexname_prefix_item, root_path, item_location, \
    character_location
from service.webservice.llamaindex.utils import get_request
from service.logger import logger
from service.webservice.llamaindex.utils import get_kol_id

itemJsons = {}
for _, dirs, _ in os.walk(root_path + '/service/uploads/'):
    for dir in dirs:
        if os.path.exists(root_path + '/service/uploads/' + dir + '/saved_item.json'):
            itemJsons[dir] = {
                "data": json.load(open(root_path + '/service/uploads/' + dir + '/saved_item.json', encoding='utf-8'))
            }


async def ItemContentRequest(request):
    try:
        data = await request.json()
        openid = data["openid"] if "openid" in data.keys() else ""
        kol_id = await get_kol_id(openid)

        if kol_id == "":
            json_data = await get_request(item_location + "?domainid=" + kol_id)
        else:
            json_data = itemJsons[kol_id]

        return web.json_response(status=200, data=json_data)
    except json.JSONDecodeError:
        return web.Response(text="Invalid JSON", status=400)
