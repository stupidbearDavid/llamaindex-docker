#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
import traceback

from aiohttp import web

from service.logger import logger
from service.indcing import add_content_index
from config import default_openid, is_test, indexname_prefix_knowledge
from service.authority import check_authority
from service.webservice.llamaindex.utils import get_kol_id

root_path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + os.path.sep + ".") + '/'


async def PushKnowledgeDataRequest(request):
    try:
        data = await request.json()
    except json.JSONDecodeError:
        return web.Response(text="Invalid JSON", status=400)
    try:
        indexname = indexname_prefix_knowledge
        business = indexname_prefix_knowledge
        # 当openid从传入是异常或者没有的时候，就用标准的Chunk数据
        # if "openid" in data.keys() and data["openid"].strip() != "":
        #     try:
        #         kol_id = await get_kol_id(data["openid"])
        #         if kol_id != "":
        #             indexname = indexname + "_" + kol_id
        #             business = business + "_" + kol_id
        #             openid = data["openid"]
        #         else:
        #             openid = default_openid
        #             kol_id = openid
        #     except Exception:
        #         logger.error(traceback.format_exc())
        #         response = {
        #             'status': 500,
        #             'message': 'Request InternetId by openId error!'
        #         }
        #         return web.json_response(status=500, data=response)
        # else:
        #     openid = default_openid
        #     kol_id = openid

        if not "domainid" in data.keys() or str(data["domainid"]).strip() == "":
            response = {
                'status': 402,
                'message': "parameter 'domainid' is missed!"
            }
            response = json.loads(json.dumps(response, ensure_ascii=False))
            return web.json_response(status=402, data=response)
        domainid = str(data["domainid"]).strip()
        if is_test:
            indexname = indexname + "_" + domainid + "_test"
            business = business + "_" + domainid + "_test"
        else:
            indexname = indexname + "_" + domainid
            business = business + "_" + domainid

        logger.info('add content: {}'.format(data))
        text = data['content']
        if 'filename' in data.keys():
            filename = data['filename']
        else:
            filename = None
        res = await add_content_index(indexname=indexname, content=text, filename=filename)
        response = {
            'status': 200,
            'message': res
        }
        response = json.loads(json.dumps(response, ensure_ascii=False))
        return web.json_response(status=200, data=response)

    except Exception:
        logger.error(traceback.format_exc())
        response = {
            'status': 500,
            'message': traceback.format_exc()
        }
        return web.json_response(status=500, data=response)
