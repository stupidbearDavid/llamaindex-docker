#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import json
import traceback

from aiohttp import web

from service.logger import logger
from service.neo4j_index import delete_all
from service.authority import get_email, get_authorize_level, authority_level

root_path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + os.path.sep + ".") + '/'


async def HealthCheckRequest(request):

    try:
        response = {
            'status': 200,
            'message': "ok!"
        }
        return web.json_response(status=200, data=response)
    except Exception:
        logger.error(traceback.format_exc())
        response = {
            'status': 500,
            'message': "Interval Error!"
        }
        return web.json_response(status=500, data=response)
