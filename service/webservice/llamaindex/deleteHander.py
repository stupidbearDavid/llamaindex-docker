#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import traceback

from aiohttp import web

from service.logger import logger
from service.indcing import delete_doc
from service.authority import get_email, is_authorized


async def DeleteRequest(request):
    try:
        data = await request.json()
    except json.JSONDecodeError:
        return web.Response(text="Invalid JSON", status=400)
    try:
        file_name = data['filename']
        index_name = data['indexname']

        await delete_doc(index_name, file_name)
        response = {
            'status': 200,
            'message': f"Delete files successfully."
        }
        response = json.loads(json.dumps(response, ensure_ascii=False))
        return web.json_response(status=200, data=response)

    except Exception:
        logger.error(traceback.format_exc())
        response = {
            'status': 500,
            'message': 'Interval Error!'
        }
        return web.json_response(status=500, data=response)