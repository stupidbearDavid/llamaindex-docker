#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
import traceback

from aiohttp import web

from service.indcing import add_index, add_QA_index_from_file
from service.authority import check_authority
from config import root_path, default_openid, is_test, indexname_prefix_live
from service.logger import logger
from service.webservice.llamaindex.utils import get_kol_id


async def PushLiveFileRequest(request):
    uploaded_file_paths = []
    data = {}
    try:
        reader = await request.multipart()
        id = None
        # 先遍历 reader 获取 id 字段
        async for part in reader:
            if part.name == "id":
                id = await part.text()  # 获取 id 字段的值
                break  # 获取到 id 后跳出循环

        if id is None:
            return web.Response(text="parameter 'id' is missed!", status=402)
        async for part in reader:
            # 检查部分是否为文件
            if part.filename:
                filename = id if id is not None else part.filename
                save_path = os.path.join(root_path + 'service/uploads', filename)

                # 保存文件内容
                with open(save_path, 'wb') as f:
                    while True:
                        chunk = await part.read_chunk()  # 读取上传文件的块
                        if not chunk:
                            break
                        f.write(chunk)

                uploaded_file_paths.append(filename)

            else:
                # 处理其他表单数据（普通字段）
                field_name = part.name
                field_value = await part.text()  # 将其读取为文本
                data[field_name] = field_value
    except json.JSONDecodeError:
        return web.Response(text="Invalid JSON", status=400)
    try:
        indexname = indexname_prefix_live
        business = indexname_prefix_live
        if not "domainid" in data.keys() or str(data["domainid"]).strip() == "":
            response = {
                'status': 402,
                'message': "parameter 'domainid' is missed!"
            }
            response = json.loads(json.dumps(response, ensure_ascii=False))
            return web.json_response(status=402, data=response)
        domainid = str(data["domainid"]).strip()
        if is_test:
            indexname = indexname + "_" + domainid + "_test"
        else:
            indexname = indexname + "_" + domainid
        business = business + "_" + domainid
        type = data["type"] if "type" in data.keys() else None

        responses = []
        for filename in uploaded_file_paths:
            if filename:
                if type is None:
                    response_str = await add_index(indexname, filename)
                elif type == "QA":
                    response_str = await add_QA_index_from_file(indexname, filename)
                else:
                    response_str = await add_index(indexname, filename)
                response = {
                    'status': 200,
                    'message': response_str
                }
                response = json.loads(json.dumps(response, ensure_ascii=False))
                logger.info("{}, {}".format(filename, response))
                responses.append(response)
        for filename in uploaded_file_paths:
            try:
                location = os.path.join(root_path + 'service/uploads', filename)
                # 删除对应的filename文件
                os.remove(location)
                logger.info(f"Successfully deleted {location}")
            except FileNotFoundError:
                print(f"File {filename} not found. It might have already been deleted.")
            except Exception as e:
                print(f"Error occurred while deleting {filename}: {e}")
        return web.json_response(status=200, data=responses)

    except Exception:
        logger.error(traceback.format_exc())
        response = {
            'status': 500,
            'message': 'Interval Error!'
        }
        return web.json_response(status=500, data=response)
