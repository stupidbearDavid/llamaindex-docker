#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
import traceback

from aiohttp import web

from service.indcing import add_index, add_content_index, delete_index
from service.webservice.llamaindex.utils import get_request
from service.logger import logger
from config import item_location, root_path, db_type, default_openid, is_test, indexname_prefix_item
from service.file_utils import load_file, save_file
from service.redis_index import delete_doc_by_metadata
from service.webservice.llamaindex.utils import get_kol_id


def check_data(old_data, data):
    # old_content = dict([[d["h5Url"], "{}, {}, {}, {}".format(d["item_name"], d["aliases"],
    #                                                          d["profile"]["efficacy"],
    #                                                          ",".join(d["keywords"].split('#')))] for d in
    #                     old_data])

    # new_content = dict([[d["h5Url"], "{}, {}, {}, {}".format(d["item_name"], d["aliases"],
    #                                                          d["profile"]["efficacy"],
    #                                                          ",".join(d["keywords"].split('#')))] for d in
    #                     data])

    old_content = {}
    for d in old_data:
        if d["profile"] is not None and d["h5Url"] is not None and d["item_name"] is not None \
                and d["aliases"] is not None and d["keywords"] is not None and d["profile"]["efficacy"] is not None:
            old_content[d["h5Url"]] = "{}, {}, {}, {}".format(
                d["item_name"],
                d["aliases"],
                d["profile"]["efficacy"],
                ",".join(d["keywords"].split('#'))
            )

    new_content = {}
    for d in data:
        if d["profile"] is not None and d["h5Url"] is not None and d["item_name"] is not None \
                and d["aliases"] is not None and d["keywords"] is not None and d["profile"]["efficacy"] is not None:
            new_content[d["h5Url"]] = "{}, {}, {}, {}".format(
                d["item_name"],
                d["aliases"],
                d["profile"]["efficacy"],
                ",".join(d["keywords"].split('#'))
            )

    need_upload = []
    for qa in new_content.keys():
        if qa not in old_content.keys():
            need_upload.append({"unique_id": qa,
                                "text": new_content[qa],
                                "business": "item_profile"})
        # 如果问题内容已经存在
        else:
            if new_content[qa] != old_content[qa]:
                need_upload.append({"unique_id": qa,
                                    "text": new_content[qa],
                                    "business": "item_profile"})
    need_delete = []
    for d in old_content.keys():
        if d not in new_content.keys():
            need_delete.append({"unique_id": d,
                                "text": old_content[d],
                                "business": "item_profile"})

    old_content = {}
    for d in old_data:
        if d["product_str"] is not None and d["h5Url"] is not None and d["aliases"] is not None:
            old_content[d["h5Url"]] = "{}, {}".format(
                d["product_str"],
                d["aliases"]
            )

    new_content = {}
    for d in data:
        if d["product_str"] is not None and d["h5Url"] is not None and d["aliases"] is not None:
            new_content[d["h5Url"]] = "{}, {}".format(
                d["product_str"],
                d["aliases"]
            )

    for qa in new_content.keys():
        if qa not in old_content.keys():
            need_upload.append({"unique_id": qa,
                                "text": new_content[qa],
                                "business": "item_profile"})
        # 如果问题内容已经存在
        else:
            if new_content[qa] != old_content[qa]:
                need_upload.append({"unique_id": qa,
                                    "text": new_content[qa],
                                    "business": "item_profile"})
    for d in old_content.keys():
        if d not in new_content.keys():
            need_delete.append({"unique_id": d,
                                "text": old_content[d],
                                "business": "item_profile"})
    return need_upload, need_delete


async def PushItemDataRequest(request):
    try:
        data = await request.json()
    except json.JSONDecodeError:
        return web.Response(text="Invalid JSON", status=400)
    try:
        logger.info("update item data: {}".format(data))
        indexname = indexname_prefix_item
        business = indexname_prefix_item
        # 当openid从传入是异常或者没有的时候，就用标准的Chunk数据
        # if "openid" in data.keys() and data["openid"].strip() != "":
        #     try:
        #         kol_id = await get_kol_id(data["openid"])
        #         if kol_id != "":
        #             indexname = indexname + "_" + kol_id
        #             business = business + "_" + kol_id
        #             openid = data["openid"]
        #         else:
        #             openid = default_openid
        #             kol_id = openid
        #     except Exception:
        #         logger.error(traceback.format_exc())
        #         response = {
        #             'status': 500,
        #             'message': 'Request InternetId by openId error!'
        #         }
        #         return web.json_response(status=500, data=response)
        # else:
        #     openid = default_openid
        #     kol_id = openid

        if not "domainid" in data.keys() or str(data["domainid"]).strip() == "":
            response = {
                'status': 402,
                'message': "parameter 'domainid' is missed!"
            }
            response = json.loads(json.dumps(response, ensure_ascii=False))
            return web.json_response(status=402, data=response)
        domainid = str(data["domainid"]).strip()
        if is_test:
            indexname = indexname + "_" + domainid + "_test"
            business = business + "_" + domainid + "_test"
        else:
            indexname = indexname + "_" + domainid
            business = business + "_" + domainid

        is_force_update = data["is_force_update"] if "is_force_update" in data.keys() else False
        json_data = await get_request(item_location + "?domainid=" + domainid)

        # openid = "93D503DCB466930ABE8B93F82B5DB960"
        json_data = json_data["data"]
        i = 0

        # 如果当前的openid不存在，那么就添加一个文件夹目录
        if not os.path.exists(os.path.join(root_path + 'service/uploads', domainid)):
            os.mkdir(os.path.join(root_path + 'service/uploads', domainid))

        if os.path.exists(
                os.path.join(root_path + 'service/uploads', domainid, "saved_item_json.json")) and not is_force_update:
            old_data = load_file(
                os.path.join(root_path + 'service/uploads', domainid, "saved_item_json.json"))
            need_upload, need_delete = check_data(old_data, json_data)

            logger.info("updated items length: {}".format(len(need_upload)))
            logger.info("delete items length: {}".format(len(need_delete)))

            # update need upload
            for d in need_upload:
                if db_type == "redis":
                    delete_doc_by_metadata(indexname, {"unique_id": d["unique_id"]}, "unique_id", d["text"])
                    logger.info("delete item: {}".format(d["unique_id"]))
                    response_str = await add_content_index(indexname, d["text"], "saved_item_json.json",
                                                           {"unique_id": d["unique_id"]})
                else:
                    response_str = await add_content_index(indexname, d, None)
            # delete need delete
            for d in need_delete:
                if db_type == "redis":
                    delete_doc_by_metadata(indexname, {"unique_id": d["unique_id"]}, "unique_id", d["text"])
                    logger.info("delete item: {}".format(d["unique_id"]))
        else:
            await delete_index(indexname)
            logger.warning("Delete {}!".format(indexname))
            need_upload, need_delete = check_data([], json_data)
            for d in need_upload:
                # content = "{}, {}, {}, {}".format(d["item_name"], d["aliases"],
                #                                   d["profile"]["efficacy"], ",".join(d["keywords"].split('#')))
                logger.info("updated items : {}".format(d["text"]))
                if db_type == "redis":
                    # response_str = await add_content_index(indexname, content, "saved_item_json.json",
                    #                                        {"unique_id": d["h5Url"]})
                    response_str = await add_content_index(indexname, d["text"], "saved_item_json.json",
                                                           {"unique_id": d["unique_id"]})
                else:
                    response_str = await add_content_index(indexname, {"unique_id": d["h5Url"],
                                                                       "text": d["text"],
                                                                       "business": "item_profile"}, None)
        save_file(json.dumps(json_data, ensure_ascii=False),
                  os.path.join(root_path + 'service/uploads', domainid,
                               "saved_item_json.json"))

        i += 1
        response = {
            'status': 200,
            'message': "ok"
        }
        response = json.loads(json.dumps(response, ensure_ascii=False))
        return web.json_response(status=200, data=response)

    except Exception:
        logger.error(traceback.format_exc())
        response = {
            'status': 500,
            'message': 'Interval Error!'
        }
        return web.json_response(status=500, data=response)
