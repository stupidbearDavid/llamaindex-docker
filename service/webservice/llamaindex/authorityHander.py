#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
import traceback

from aiohttp import web

from service.indcing import get_indexlist
from service.authority import get_email, get_authorize_level, authority_level, get_authoritylist

root_path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + os.path.sep + ".") + '/'


async def AuthorityRequest(request):
    try:
        data = await request.json()
    except json.JSONDecodeError:
        return web.Response(text="Invalid JSON", status=400)
    try:
        if 'token' in data.keys():
            accesstoken = data['token']
            email = await get_email(accesstoken)
            indexlists = get_indexlist()
            resdata = {}
            mp = get_authorize_level("llamaindex_managers", email)
            for indexname in indexlists.keys():
                if get_authorize_level(indexname, email) >= authority_level['pushfile'] or \
                        mp >= authority_level['pushfile']:
                    resdata[indexname] = [filename for filename in indexlists[indexname].keys()]
            response_str = json.loads(json.dumps(resdata, ensure_ascii=False))
            return web.json_response(status=200, data=response_str)
        else:
            response = {
                'status': 401,
                'message': "Authority deny!"
            }
            return web.json_response(status=401, data=response)
    except Exception:
        print(traceback.format_exc())
        response = {
            'status': 500,
            'message': traceback.format_exc()
        }
        return web.json_response(status=500, data=response)
