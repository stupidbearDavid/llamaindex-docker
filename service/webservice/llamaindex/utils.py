import requests
import json
import aiohttp
from config import character_location
from service.logger import logger

api_url = 'https://mentalgpt.cc/api/auth/llamaindex'

# url = 'http://192.168.31.172:8080/api/auth/llamaindex'


async def post_request(url, headers=None, data=None):
    logger.info("[post request] {}".format(url))
    # print(url, data)
    # response = await requests.post(url, headers=headers, data=json.dumps(data), timeout=60)
    # return response
    if headers is not None:
        headers["Content-Type"] = "application/json"
    else:
        headers = {"Content-Type": "application/json"}
    async with aiohttp.ClientSession() as session:
        async with session.post(url, headers=headers, data=json.dumps(data), timeout=60) as response:
            return await response.json()


async def get_request(url, headers=None, data=None):
    logger.info("[get request] {}".format(url))
    # print(url, data)
    # response = await requests.get(url, headers=headers, params=json.dumps(data), timeout=60)
    # return response
    if headers is not None:
        headers["Content-Type"] = "application/json"
    else:
        headers = {"Content-Type": "application/json"}
    async with aiohttp.ClientSession() as session:
        async with session.get(url, headers=headers, params=data, timeout=60) as response:
            return await response.json()


async def get_kol_id(openid):
    json_data = await get_request(character_location + "?openId=" + openid)
    if json_data["data"] is None:
        return ""
    return str(json_data["data"])
