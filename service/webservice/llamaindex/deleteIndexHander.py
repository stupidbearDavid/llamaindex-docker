#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import json
import traceback

from aiohttp import web
from service.logger import logger
from service.indcing import delete_index
from service.authority import get_email, is_authorized

root_path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + os.path.sep + ".") + '/'


async def DeleteIndexRequest(request):
    try:
        data = await request.json()
    except json.JSONDecodeError:
        return web.Response(text="Invalid JSON", status=400)
    try:
        logger.info('delete index: {}'.format(data))
        index_name = data['indexname']
        await delete_index(index_name)
        response = {
            'status': 200,
            'message': f"Index with name {index_name} deleted successfully."
        }
        response = json.loads(json.dumps(response, ensure_ascii=False))
        return web.json_response(status=200, data=response)

    except Exception:
        logger.error(traceback.format_exc())
        response = {
            'status': 500,
            'message': 'Interval Error!'
        }
        return web.json_response(status=500, data=response)