#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
import traceback
import asyncio
from aiohttp import web

from service.logger import logger

from service.webservice.llamaindex.queryLiveChunkHander import query_without_auth as query_live
from service.webservice.llamaindex.queryChunkHander import query_without_auth as query_knowledge
from service.webservice.llamaindex.queryRelationHander import query_without_auth as query_relation

root_path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + os.path.sep + ".") + '/'


async def extract_json_from_response(response):
    # 将响应体读取为字节
    body = response.body
    # 将字节数组解析为JSON
    try:
        return json.loads(body)["data"]
    except Exception:
        return []


async def query_without_auth(data):
    try:
        # 并发执行三个异步操作
        live_task = query_live(data)
        knowledge_task = query_knowledge(data)
        relation_task = query_relation(data)

        responses = await asyncio.gather(live_task, knowledge_task, relation_task)

        # 从响应对象中提取数据
        live_data = await extract_json_from_response(responses[0])
        knowledge_data = await extract_json_from_response(responses[1])
        relation_data = await extract_json_from_response(responses[2])

        response = {
            'status': 200,
            'data': {
                'live': live_data,
                'knowledge': knowledge_data,
                'relation': relation_data
            }
        }
        return web.json_response(status=200, data=response)
    except Exception:
        logger.error(traceback.format_exc())
        response = {
            'status': 500,
            'data': 'Interval Error'
        }
        return web.json_response(status=500, data=response)


async def QueryKnowledgeRequest(request):
    try:
        data = await request.json()
    except json.JSONDecodeError:
        return web.Response(text="Invalid JSON", status=400)
    try:
        return await query_without_auth(data)
    except Exception:
        logger.error(traceback.format_exc())
        response = {
            'status': 500,
            'message': traceback.format_exc()
        }
        return web.json_response(status=500, data=response)
