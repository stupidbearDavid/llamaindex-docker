#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import json
import traceback

from aiohttp import web

from service.logger import logger
from service.indcing import new_index
from service.authority import get_email, get_authorize_level, authority_level

root_path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + os.path.sep + ".") + '/'


async def NewIndexRequest(request):
    try:
        data = await request.json()
    except json.JSONDecodeError:
        return web.Response(text="Invalid JSON", status=400)
    try:
        index_name = data['indexname']
        # if 'token' in data.keys():
        #     accesstoken = data['token']
        #     email = await get_email(accesstoken)
        #     mp = get_authorize_level("llamaindex_managers", email)
        #     if get_authorize_level("llamaindex_managers", email) >= authority_level['manager'] or mp >= \
        #             authority_level['manager']:
        await new_index(index_name)
        response = {
            'status': 200,
            'message': f"New index with name {index_name} created successfully."
        }
        response = json.loads(json.dumps(response, ensure_ascii=False))
        return web.json_response(status=200, data=response)
    except Exception:
        logger.info(traceback.format_exc())
        response = {
            'status': 500,
            'message': 'Interval Error!'
        }
        return web.json_response(status=500, data=response)
