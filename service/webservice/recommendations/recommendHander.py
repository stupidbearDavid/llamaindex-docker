#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import traceback

import time
from aiohttp import web

from config import db_type, is_keywords_filter, rec_similarity_cutoff, is_smooth_score, is_divided_query
from service.logger import logger
from service.model import indexStore
from service.webservice.llamaindex.queryItemHander import rec_stratege_from_message


def rerank_response(query, answers, similarity_cutoff):
    rerank_results = indexStore.rerank(query, answers)
    # rerank_results = indexStore.reranker_model.rerank(query, answers)
    filtered_scores = [x for x in rerank_results["rerank_scores"] if x >= similarity_cutoff]
    result = []
    for i in range(len(filtered_scores)):
        result.append([rerank_results["rerank_passages"][i], rerank_results["rerank_scores"][i]])
    return result


async def query_without_auth(data):
    try:
        index_name = data["indexname"] if "indexname" in data.keys() else "items"
        similarity_cutoff = data["similarity_cutoff"] if "similarity_cutoff" in data.keys() else 0.0
        similarity_top_k = data["similarity_top_k"] if "similarity_top_k" in data.keys() else 20
        is_rerank = data["is_rerank"] if "is_rerank" in data.keys() else True

        logger.info('【recommedation query】: {}'.format(data))
        if "messages" in data.keys():
            messages = data["messages"]
        else:
            response = {
                'status': 400,
                'data': {}
            }
            response = json.loads(json.dumps(response, ensure_ascii=False))
            return web.json_response(status=400, data=response)

        result, details = await rec_stratege_from_message(messages, index_name,
                                                          similarity_cutoff, similarity_top_k, is_rerank)
        response = {
            'status': 200,
            'data': result
        }
        response = json.loads(json.dumps(response, ensure_ascii=False))
        logger.info('【data query result】: {}'.format(result))
        return web.json_response(status=200, data=response)
    except Exception:
        logger.info(traceback.format_exc())
        response = {
            'status': 500,
            'data': 'Interval Error'
        }
        return web.json_response(status=500, data=response)


async def QueryItemRequest(request):
    try:
        data = await request.json()
    except json.JSONDecodeError:
        return web.Response(text="Invalid JSON", status=400)
    try:
        return await query_without_auth(data)
    except Exception:
        logger.info(traceback.format_exc())
        response = {
            'status': 500,
            'message': traceback.format_exc()
        }
        return web.json_response(status=500, data=response)
