# -*- coding: utf-8 -*-

from typing import (
    Any,
    Callable,
    Dict,
    Iterable,
    List,
    Optional,
    Tuple,
    Type,
)
from config import db_type
if db_type == "redis":
    import service.redis_index as redisIndex
else:
    import service.neo4j_index as neo4jIndex

from config import db_type

email_account = []


def get_indexlist():
    if db_type == "redis":
        return redisIndex.get_indexlist()
    elif db_type == "neo4j":
        return []
    else:
        return redisIndex.get_indexlist()


def switch_index(indexname):
    if db_type == "redis":
        return redisIndex.switch_index(indexname)
    elif db_type == "neo4j":
        return neo4jIndex.switch_index(indexname)
    else:
        return redisIndex.switch_index(indexname)


def new_index(indexname):
    if db_type == "redis":
        return redisIndex.new_index(indexname)
    elif db_type == "neo4j":
        return
    else:
        return redisIndex.new_index(indexname)


async def add_index(indexname, filename):
    if db_type == "redis":
        return redisIndex.add_index(indexname, filename)
    elif db_type == "neo4j":
        return await neo4jIndex.create_documents(filename, indexname)
    else:
        return redisIndex.add_index(indexname, filename)


def fold_string(input_string, chunk_size):
    # 检查 chunk_size 是否大于 0
    if chunk_size <= 0:
        raise ValueError("Chunk size must be greater than 0.")
    # 使用列表推导式折叠字符串
    return [input_string[i:i + chunk_size] for i in range(0, len(input_string), chunk_size)]


def add_relation_index(from_label, from_props, from_key, to_label, to_props, to_key, relationship):
    if db_type == "redis":
        return
    elif db_type == "neo4j":
        return neo4jIndex.add_relationship(from_label, from_props, from_key, to_label, to_props, to_key,
                                                 relationship)
    else:
        return


async def add_content_index(indexname, content, filename, metadata=None):
    if db_type == "redis":
        return redisIndex.add_content_index(indexname, content, filename, metadata)
    elif db_type == "neo4j":
        return await neo4jIndex.add_content_index(content, indexname)
    else:
        return redisIndex.add_content_index(indexname, content, filename, metadata)


async def add_QA_index_from_file(indexname, filename):
    if db_type == "redis":
        return redisIndex.add_QA_index_from_file(indexname, filename)
    elif db_type == "neo4j":
        return await neo4jIndex.add_QA_index_from_file(indexname, filename)
    else:
        return redisIndex.add_QA_index_from_file(indexname, filename)


async def add_QA_from_file_one_by_one(INDEX, Q, A, filename, file_id, metadata=None):
    if db_type == "redis":
        return redisIndex.add_QA_from_file_one_by_one(INDEX, Q, A, filename, file_id, metadata)
    elif db_type == "neo4j":
        return await neo4jIndex.acreate_QA(Q, A, INDEX.node_label)
    else:
        return redisIndex.add_QA_from_file_one_by_one(INDEX, Q, A, filename, file_id, metadata)


async def add_QA_index(indexname, Q, A, filename, metadata=None):
    if db_type == "redis":
        return redisIndex.add_QA_index(indexname, Q, A, filename, metadata)
    elif db_type == "neo4j":
        return await neo4jIndex.acreate_QA(Q, A, indexname)
    else:
        return redisIndex.add_QA_index(indexname, Q, A, filename, metadata)


async def get_doc(indexname, filename, metadata: Optional[Any] = {}, start_index: Optional[int] = 0,
                  limit: Optional[int] = 0):
    if db_type == "redis":
        return redisIndex.get_doc(indexname, filename)
    elif db_type == "neo4j":
        return await neo4jIndex.get_doc(indexname, metadata, start_index, limit)
    else:
        return redisIndex.get_doc(indexname, filename)


async def delete_index(indexname):
    if db_type == "redis":
        return redisIndex.delete_index(indexname)
    elif db_type == "neo4j":
        return await neo4jIndex.delete_index(indexname)
    else:
        return redisIndex.delete_index(indexname)


async def delete_doc(indexname, filename):
    if db_type == "redis":
        return redisIndex.delete_doc(indexname, filename)
    elif db_type == "neo4j":
        return
    else:
        return redisIndex.delete_doc(indexname, filename)


async def index_query(INDEX, query, similarity_top_k, similarity_cutoff, metadata=None):
    if db_type == "redis":
        return redisIndex.query(INDEX, query, similarity_top_k, similarity_cutoff)
    elif db_type == "neo4j":
        return await neo4jIndex.query(INDEX, query, similarity_top_k, similarity_cutoff, metadata)
    else:
        return redisIndex.query(INDEX, query, similarity_top_k, similarity_cutoff)


if __name__ == '__main__':
    print("这是一个测试")
