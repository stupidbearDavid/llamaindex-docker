# -*- coding: utf-8 -*-

import os
import json
import traceback
import hashlib
import time
import shutil
import struct

from llama_index.vector_stores import RedisVectorStore
from llama_index.vector_stores.types import VectorStoreQueryMode

from service.logger import logger
from service.model import Singleton, url

from llama_index import SimpleDirectoryReader, VectorStoreIndex, Document

from service.authority import get_authoritylist, vector_store
from config import root_path, cuda_num

email_account = []


def get_md5_hash(time_str):
    md5_hash = hashlib.md5(time_str.encode('utf-8')).hexdigest()
    return md5_hash


def float_array_to_bytes(float_array):
    byte_array = b''
    for num in float_array:
        byte_array += struct.pack('f', num)
    return byte_array


def bytes_to_float_array(byte_array):
    float_array = []
    num_bytes = len(byte_array) // 4  # 每个float占用4个字节

    for i in range(num_bytes):
        float_value = struct.unpack('f', byte_array[i * 4:(i + 1) * 4])[0]
        float_array.append(float_value)

    return float_array


def get_embedding_feature(text):
    return Singleton.instance().instance["embedding_model"].get_text_embedding(text)


def get_indexlist():
    if Singleton.indexlist_vector_store.client.exists("index_list"):
        data = Singleton.indexlist_vector_store.client.get("index_list")
        index_list = json.loads(data)
    else:
        Singleton.indexlist_vector_store.client.append("index_list", "{\"" + "llamaindex" + "\": {}}")
        index_list = {"\"" + "llamaindex" + "\"": {}}
    return index_list


def switch_index(indexname):

    vector_store = RedisVectorStore(
        index_name=indexname,
        index_prefix=indexname,
        redis_url=url,
        overwrite=False
    )
    INDEX = VectorStoreIndex.from_vector_store(vector_store=vector_store,
                                               service_context=Singleton.instance().instance["service_context"])
    if not Singleton.indexlist_vector_store.client.exists("index_list"):
        Singleton.indexlist_vector_store.client.append("index_list", "{\"" + indexname + "\": {}}")
    index_list = get_indexlist()
    if indexname not in index_list.keys():
        new_index(indexname)
    logger.info("switch index to: {}".format(indexname))
    return INDEX


def new_index(indexname):
    index_list = get_indexlist()
    if indexname in index_list.keys():
        logger.info("index: {} already exists!".format(indexname))
        return
    index_list[indexname] = {}
    authortiy_list = get_authoritylist()
    if indexname in authortiy_list.keys():
        logger.info("index: {} already exists!".format(indexname))
        return
    authortiy_list[indexname] = {}
    vector_store.client.set("authority_list", json.dumps(authortiy_list))
    Singleton.indexlist_vector_store.client.set("index_list", json.dumps(index_list))
    logger.info("switch index is started...")


def add_index(indexname, filename):
    INDEX = switch_index(indexname)
    documents = SimpleDirectoryReader(input_files=[root_path + 'service/uploads/' + filename]).load_data()
    index_list = get_indexlist()
    chunks = [documents[0].text]
    if filename in index_list[indexname].keys():
        logger.info("File: \'" + filename + "\' is already existed!")
        delete_doc(indexname, filename)
        logger.info("File: \'" + filename + "\' is deleted!")
    for document in documents[1:]:
        chunks.append(document.text)
    start = 0
    document = Document(text=chunks[0], file_name=filename, metadata={"file_name": filename}, start_char_idx=start)

    if not document.text == "":
        INDEX.insert(document)
        start += len(chunks[0])
    else:
        return "Empty content: \'" + filename + "\'!"
    ref_id = document.get_doc_id()
    index_list[indexname][filename] = ref_id
    Singleton.indexlist_vector_store.client.set("index_list", json.dumps(index_list))
    for chunk in chunks[1:]:
        if document.text == "":
            continue
        doc = Document(text=chunk, doc_id=ref_id,
                       ref_doc_id=ref_id,
                       file_name=filename, metadata={"file_name": filename}, start_char_idx=start)
        INDEX.insert(doc)
        del doc
        start += len(chunk)
    return document.get_doc_id()


def fold_string(input_string, chunk_size):
    # 检查 chunk_size 是否大于 0
    if chunk_size <= 0:
        raise ValueError("Chunk size must be greater than 0.")
    # 使用列表推导式折叠字符串
    return [input_string[i:i + chunk_size] for i in range(0, len(input_string), chunk_size)]


def add_content_index(indexname, content, filename, metadata):
    INDEX = switch_index(indexname)
    index_list = get_indexlist()
    chunks = fold_string(content, 4096)

    start = 0
    if filename is None:
        filename = get_md5_hash(str(time.time()))
        document = Document(text=chunks[0], file_name=filename,
                            metadata={"file_name": filename} if metadata is None else
                            {"file_name": filename, "metadata": json.dumps(metadata)}, start_char_idx=start)
    else:
        if filename not in index_list[indexname]:
            document = Document(text=chunks[0], file_name=filename,
                                metadata={"file_name": filename} if metadata is None else
                                {"file_name": filename, "metadata": json.dumps(metadata)},
                                start_char_idx=start)
        else:
            document = Document(text=chunks[0], doc_id=index_list[indexname][filename],
                                ref_doc_id=index_list[indexname][filename],
                                file_name=filename, metadata={"file_name": filename} if metadata is None else
                {"file_name": filename, "metadata": json.dumps(metadata)}, start_char_idx=start)

    if not document.text == "":
        INDEX.insert(document)
        start += len(chunks[0])
    else:
        return "Empty content: \'" + filename + "\'!"
    ref_id = document.get_doc_id()
    for chunk in chunks[1:]:
        if document.text == "":
            continue
        INDEX.insert(Document(text=chunk, doc_id=ref_id,
                              ref_doc_id=ref_id,
                              file_name=filename, metadata={"file_name": filename} if metadata is None else
            {"file_name": filename, "metadata": json.dumps(metadata)}, start_char_idx=start))
        start += len(chunk)
    # index_list[indexname][filename] = document.get_doc_id()
    # indexlist_vector_store.client.set("index_list", json.dumps(index_list))
    return document.get_doc_id()


def add_QA_index_from_file(indexname, filename):
    INDEX = switch_index(indexname)
    index_list = get_indexlist()
    if filename in index_list[indexname].keys():
        return "File: \'" + filename + "\' is already existed!"
    file_id = get_md5_hash(str(time.time()))
    index_list[indexname][filename] = file_id
    with open(root_path + 'service/uploads/' + filename, encoding="utf-8") as data:
        for line in data:
            if line.strip() == "":
                continue
            logger.info(line)
            Q, A, meta = line.split('\t')
            if Q == "" or A == "":
                logger.info("[bad content] {}, {}, {}".format(Q, A, line))
                continue
            try:
                metadata = json.loads(meta.strip())
            except:
                logger.error("[bad metadata] {}".format(meta))
                metadata = None
            # add_QA_index(indexname, Q, A, filename)
            add_QA_from_file_one_by_one(INDEX, Q, A, filename, file_id, metadata)
    Singleton.indexlist_vector_store.client.set("index_list", json.dumps(index_list))
    return file_id


def add_QA_from_file_one_by_one(INDEX, Q, A, filename, file_id, metadata):
    document = Document(text=Q, doc_id=file_id,
                        ref_doc_id=file_id,
                        file_name=filename, metadata={"file_name": filename, "QA_answer": A} if metadata is None else
        {"file_name": filename, "QA_answer": A, "metadata": json.dumps(metadata)},
                        start_char_idx=None)
    result = query(INDEX, Q, 1, 0.999)
    if len(result) > 0:
        INDEX.vector_store.delete(ref_doc_id=result[0][3]["ref_doc_id"])
    if not document.text == "":
        INDEX.insert(document)
    else:
        logger.info("Empty content: \'" + filename + "\'!")


def add_QA_index(indexname, Q, A, filename, metadata):
    INDEX = switch_index(indexname)
    index_list = get_indexlist()
    chunks = fold_string(Q, 4096)
    start = 0
    try:
        result = query(INDEX, Q, 1, 0.999)
        if len(result) > 0:
            INDEX.vector_store.delete(ref_doc_id=result[0][3]["ref_doc_id"])
    except Exception:
        logger.error(traceback.format_exc())
    if filename is None:
        filename = get_md5_hash(str(time.time()))
        document = Document(text=chunks[0], file_name=filename,
                            metadata={"file_name": filename, "QA_answer": A} if metadata is None else
                            {"file_name": filename, "QA_answer": A, "metadata": json.dumps(metadata)},
                            start_char_idx=None)
    else:
        if filename not in index_list[indexname]:
            document = Document(text=chunks[0], file_name=filename,
                                metadata={"file_name": filename, "QA_answer": A} if metadata is None else
                                {"file_name": filename, "QA_answer": A, "metadata": json.dumps(metadata)},
                                start_char_idx=None)
        else:
            document = Document(text=chunks[0], doc_id=index_list[indexname][filename],
                                ref_doc_id=index_list[indexname][filename],
                                file_name=filename,
                                metadata={"file_name": filename, "QA_answer": A} if metadata is None else
                                {"file_name": filename, "QA_answer": A, "metadata": json.dumps(metadata)},
                                start_char_idx=None)

    if not document.text == "":
        INDEX.insert(document)
        start += len(chunks[0])
    else:
        logger.info("Empty content: \'" + filename + "\'!")
    ref_id = document.get_doc_id()
    for chunk in chunks[1:]:
        if document.text == "":
            continue
        INDEX.insert(Document(text=chunk, doc_id=ref_id,
                              ref_doc_id=ref_id,
                              file_name=filename,
                              metadata={"file_name": filename, "QA_answer": A} if metadata is None else
                              {"file_name": filename, "QA_answer": A, "metadata": json.dumps(metadata)},
                              start_char_idx=None))
        start += len(chunk)
    # index_list[indexname][filename] = document.get_doc_id()
    # indexlist_vector_store.client.set("index_list", json.dumps(index_list))
    return document.get_doc_id()


def get_doc(indexname, filename):
    INDEX = switch_index(indexname)
    doc_id = get_indexlist()[indexname][filename]
    results = {}
    keys = INDEX.vector_store.get_docs(doc_id)
    for key in keys:
        results[key["id"]] = key["text"]
    return results


def delete_index(indexname):
    index_list = get_indexlist()
    if indexname == "llamaindex":
        return
    if not indexname in index_list.keys():
        return
    filelist = index_list.pop(indexname)
    for filename in filelist:
        delete_doc(indexname, filename)
    INDEX = switch_index(indexname)
    try:
        INDEX.vector_store.delete_index()
    except Exception:
        logger.info(traceback.format_exc())
    Singleton.indexlist_vector_store.client.set("index_list", json.dumps(index_list))
    authortiy_list = get_authoritylist()
    if indexname in authortiy_list.keys():
        authortiy_list.pop(indexname)
    vector_store.client.set("authority_list", json.dumps(authortiy_list))
    logger.info("Finished delete index: \'" + indexname + "\'!")


def delete_doc(indexname, filename):
    INDEX = switch_index(indexname)
    index_list = get_indexlist()
    ref_doc_id = index_list[indexname].pop(filename)
    try:
        INDEX.vector_store.delete(ref_doc_id)
        image_path = root_path + 'service/uploads/' + get_md5_hash(filename)
        if os.path.exists(image_path):
            shutil.rmtree(image_path)
        file_name = root_path + 'service/uploads/' + filename
        if os.path.exists(file_name):
            os.remove(file_name)
    except Exception:
        logger.info(traceback.format_exc())
    Singleton.indexlist_vector_store.client.set("index_list", json.dumps(index_list))
    logger.info("Finished delete file: \'" + filename + "\' from " + indexname + "!")


def delete_doc_by_ref_doc_id(INDEX, ref_doc_id):
    INDEX.vector_store.delete(ref_doc_id=ref_doc_id)


def delete_doc_by_metadata(indexname, metadata, key, text):
    INDEX = switch_index(indexname)
    result = query(INDEX, text, 10, 0.9)
    for r in result:
        if r[2][key] == metadata[key]:
            delete_doc_by_ref_doc_id(INDEX, r[2]["ref_doc_id"])


def delete_doc_by_similiarity(indexname, text):
    INDEX = switch_index(indexname)
    result = query(INDEX, text, 10, 0.99)
    for r in result:
        delete_doc_by_ref_doc_id(INDEX, r[3]["ref_doc_id"])


def query(INDEX, query, similarity_top_k, similarity_cutoff):
    engine = INDEX.as_query_engine(vector_store_query_mode=VectorStoreQueryMode.DEFAULT,
                                   similarity_top_k=similarity_top_k,
                                   vector_store_kwargs={'similarity_cutoff': similarity_cutoff})
    try:
        passages = engine.query(query).response
        return passages
    except Exception:
        logger.error(traceback.format_exc())
        return []


if __name__ == '__main__':
    INDEX = switch_index("items_2")
    result = query(INDEX, "菜瓜瓜", 20, 0.0)
    logger.info(get_embedding_feature("这是一个测试"))


