import math
import time
from neo4j.exceptions import ConstraintError

from service.logger import logger
from langchain_community.graphs import Neo4jGraph
from langchain_community.vectorstores import Neo4jVector
from config import neo4j_pwd, neo4j_port, neo4j_url, neo4j_user, neo4j_db, chunk_size, overlap_size, db_type
from service.model import Singleton, root_path
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.document_loaders import CSVLoader, PyPDFLoader, TextLoader, Docx2txtLoader
from service.redis_index import get_md5_hash

NEO4J_URI = "neo4j://{}:{}".format(neo4j_url, neo4j_port)

VECTOR_SOURCE_PROPERTY = 'text'
VECTOR_EMBEDDING_PROPERTY = 'textEmbedding'
NEO4J_USERNAME = neo4j_user
NEO4J_PASSWORD = neo4j_pwd

kg = None
if db_type == "neo4j":
    kg = Neo4jGraph(
        url=NEO4J_URI, username=NEO4J_USERNAME, password=NEO4J_PASSWORD, database=neo4j_db
    )

text_splitter = RecursiveCharacterTextSplitter(chunk_size=chunk_size, chunk_overlap=overlap_size)

"""
1）business 作为metadata属性, 用于区分不同的节点任务
2）在QA应用中，node_label标准化成FAQ，根据其他应用改变node_label
3）通过metadata中的属性来进行类别划分
"""


def score_smooth(score):
    return 0.5 * math.tanh((1.2 * score - 0.75) * 5) + 0.5


def check_node(node_label, pairwise):
    pairwise_conditions = ",".join(["{key}: '{value}'".format(key=key, value=value) for key, value in pairwise.items()])
    # 首先检查是否存在具有相同id的节点
    check_query = f"""
    MATCH (n:{node_label} {{{pairwise_conditions}}})
    RETURN n
    """
    result = kg.query(check_query)

    return result


def check_relation(node1_label, node1_unique_id, from_key, node2_label, node2_unique_id, to_key):
    if node1_label is None and node1_unique_id is None and from_key is None:
        check_query = f"""
            MATCH ()-[r]->(n2:{node2_label} {{{to_key}: "{node2_unique_id}"}})
            RETURN r
            """
    elif node2_label is None and node2_unique_id is None and to_key is None:
        check_query = f"""
            MATCH (n1:{node1_label} {{{from_key}: "{node1_unique_id}"}})-[r]->()
            RETURN r
            """
    else:
        check_query = f"""
        MATCH (n1:{node1_label} {{{from_key}: "{node1_unique_id}"}})-[r]->(n2:{node2_label} {{{to_key}: "{node2_unique_id}"}})
        RETURN r
        """
    result = kg.query(check_query)

    if len(result) > 0:
        logger.info(f"Relationship between {node1_label} and {node2_label} already exists.")
        return True

    return False


def add_relationship(from_label, from_props, from_key, to_label, to_props, to_key, relationship, r_property=None):
    if not check_relation(from_label, from_props[from_key], from_key, to_label, to_props[to_key], to_key):
        if r_property is None:
            cypher_query = f"""
                MATCH (a:{from_label} {{{from_key}: "{from_props[from_key]}"}}), 
                (b:{to_label} {{{to_key}: "{to_props[to_key]}"}})
                CREATE (a)-[r:{relationship}]->(b)
                RETURN r
                """
        else:
            cypher_query = f"""
                MATCH (a:{from_label} {{{from_key}: "{from_props[from_key]}"}}), 
                (b:{to_label} {{{to_key}: "{to_props[to_key]}"}})
                CREATE (a)-[r:{relationship} {{{r_property}}}]->(b)
                RETURN r
                """

        kg.query(cypher_query)


def file_loader(file_name):
    tail_fix = file_name.split('.')[-1]
    if tail_fix == "pdf":
        loader = PyPDFLoader(file_name)
    elif tail_fix == "csv":
        loader = CSVLoader(file_name, encoding="utf-8")
    elif tail_fix == "txt":
        loader = TextLoader(file_name, encoding="utf-8")
    elif tail_fix == "docx":
        loader = Docx2txtLoader(file_name)
    else:
        loader = TextLoader(file_name, encoding="utf-8")
    return loader.load_and_split(
        text_splitter=text_splitter)


def create_documents(filename, node_label="Chunk"):
    """
    for chunk usage
    """
    documents = file_loader(root_path + 'service/uploads/' + filename)

    neo4j_vector_store = switch_index(node_label)
    neo4j_vector_store.from_documents(documents=documents,
                                      embedding=Singleton.instance().embedding_model,
                                      url=NEO4J_URI,
                                      username=NEO4J_USERNAME,
                                      password=NEO4J_PASSWORD,
                                      index_name="{}_vector".format(node_label),
                                      node_label=node_label,
                                      text_node_property=VECTOR_SOURCE_PROPERTY,
                                      embedding_node_property=VECTOR_EMBEDDING_PROPERTY)


async def add_content_index(content, node_label="Chunk"):
    """
        add vector indexed node
    """
    INDEX = switch_index(node_label)
    try:
        await INDEX.aadd_texts(texts=[content["text"]],
                               metadatas=[content],
                               node_label=node_label,
                               text_node_property=VECTOR_SOURCE_PROPERTY,
                               embedding_node_property=VECTOR_EMBEDDING_PROPERTY)
    except ConstraintError:
        logger.info("[ConstraintError] {}".format(content))


def switch_index(node_label):
    new_neo4j_vector_store = Neo4jVector(
        embedding=Singleton.instance().instance["embedding_model"],
        url=NEO4J_URI,
        username=NEO4J_USERNAME,
        password=NEO4J_PASSWORD,
        index_name="{}_vector".format(node_label),
        node_label=node_label,
        text_node_property=VECTOR_SOURCE_PROPERTY,
        embedding_node_property=VECTOR_EMBEDDING_PROPERTY,
    )
    create_node_index(new_neo4j_vector_store)
    return new_neo4j_vector_store


def create_node_index(neo4j_vector_store):
    # Check if the vector index already exists
    embedding_dimension, index_type = neo4j_vector_store.retrieve_existing_index()
    if not embedding_dimension:
        neo4j_vector_store.create_new_index()
        # create_full_text_index(neo4j_vector_store.node_label)
        create_unique_index(neo4j_vector_store.node_label, "unique_id")
        if neo4j_vector_store.node_label == "FAQ":
            create_unique_index(neo4j_vector_store.node_label, "type")
        create_unique_index(neo4j_vector_store.node_label, "business")
        if neo4j_vector_store.node_label == "人物":
            create_unique_index(neo4j_vector_store.node_label, "name")
            create_full_text_index(neo4j_vector_store.node_label, "neck_name")
        # create_unique_index(neo4j_vector_store.node_label, "text")


def create_full_text_index(node_label, property_name=VECTOR_SOURCE_PROPERTY):
    index_name = "{}_fulltext".format(node_label)
    vertex_type = node_label  # 确保在Neo4j中你使用的标签名称大小写匹配

    # 创建索引的Cypher查询
    cypher_query_create = f"""
    CREATE FULLTEXT INDEX {index_name}
        IF NOT EXISTS
        FOR (n:{vertex_type})
        ON EACH [n.{property_name}]
    """

    logger.info(cypher_query_create)
    # 尝试执行Cypher查询来创建索引
    try:
        result = kg.query(cypher_query_create)
        return result
    except Exception as e:
        # 处理异常，可能是因为索引已经存在或其他任何原因
        return str(e)


def create_unique_index(node_label, property_name):
    index_name = "{}_unique".format(node_label)
    vertex_type = node_label  # 确保在Neo4j中你使用的标签名称大小写匹配

    # 创建索引的Cypher查询
    cypher_query_create = f"""
    CREATE CONSTRAINT {index_name}
        IF NOT EXISTS
        FOR (n:{vertex_type})
        REQUIRE n.{property_name} IS UNIQUE
    """

    logger.info(cypher_query_create)
    # 尝试执行Cypher查询来创建索引
    try:
        result = kg.query(cypher_query_create)
        return result
    except Exception as e:
        # 处理异常，可能是因为索引已经存在或其他任何原因
        return str(e)


async def add_QA_index_from_file(business, filename):
    fileid = get_md5_hash(filename)
    with open(root_path + 'service/uploads/' + filename, encoding="utf-8") as data:
        for line in data:
            if line.strip() == "":
                continue
            logger.info(line)
            id, Q, A = line.strip().split('\t')
            if len(line.strip().split('\t')) != 3 or Q == "" or A == "":
                logger.info("[bad content]", line)
                continue
            await acreate_QA({"unique_id": "{}_{}_question".format(fileid, id), "text": Q,
                              "business": business, "filename": filename, "fileid": fileid},
                             {"unique_id": "{}_{}_answer".format(fileid, id), "text": A,
                              "business": business, "filename": filename, "fileid": fileid},
                             business)
    return fileid


async def acreate_QA(Q, A, business):
    store = switch_index("FAQ")
    Q_node = check_node(node_label="FAQ", pairwise={"text": Q["text"], "business": business})
    # 没有找到问题内容节点
    if len(Q_node) == 0:
        Q["type"] = "Questions"
        try:
            await store.aadd_texts(texts=[Q["text"]],
                                   metadatas=[Q],
                                   node_label="FAQ",
                                   text_node_property=VECTOR_SOURCE_PROPERTY,
                                   embedding_node_property=VECTOR_EMBEDDING_PROPERTY)
        except ConstraintError:
            logger.info("[ConstraintError] {}".format(Q))
    results = await store.asimilarity_search_with_relevance_scores(A["text"], k=1, score_threshold=0.9999,
                                                                   filter={"business": business})
    # 如果检索有答案，就直接创建结点关联
    if len(results) == 1:
        add_relationship("FAQ", results[0][0].metadata, "unique_id", "FAQ", Q, "unique_id", "ANSWER")
    # 如果没有答案，就创建节点，后添加节点关联
    else:
        A["type"] = "Answers"
        question_node = Q_node[0]['n'] if len(Q_node) > 0 else Q
        if check_relation(None, None, None, "FAQ", question_node["unique_id"], "unique_id"):
            delete_relationship(None, None, None, "FAQ", question_node["unique_id"], "unique_id")

        try:
            await store.aadd_texts(texts=[A["text"]],
                                   metadatas=[A],
                                   node_label="FAQ",
                                   text_node_property=VECTOR_SOURCE_PROPERTY,
                                   embedding_node_property=VECTOR_EMBEDDING_PROPERTY)
        except ConstraintError:
            logger.info("[ConstraintError] {}".format(A))
        add_relationship("FAQ", A, "unique_id", "FAQ", question_node, "unique_id", "ANSWER")


async def aquery(neo4j_vector_store, query, similiary_top_k, similiary_cutoff, metadata):
    if "query_type" in metadata.keys():
        del metadata["query_type"]
    retriever = await neo4j_vector_store.asimilarity_search_with_relevance_scores(query,
                                                                                  k=similiary_top_k,
                                                                                  filter=metadata,
                                                                                  score_threshold=similiary_cutoff)
    questions = {}
    for r in retriever:
        score = r[1]
        questions[r[0].page_content] = [r[0].metadata["unique_id"], score]
    return questions


async def query_QA(neo4j_vector_store, query, similiary_top_k, similiary_cutoff, metadata):
    metadata["type"] = "Questions"
    if "query_type" in metadata.keys():
        del metadata["query_type"]
    retriever = await neo4j_vector_store.asimilarity_search_with_relevance_scores(query,
                                                                                  k=similiary_top_k,
                                                                                  filter=metadata,
                                                                                  score_threshold=similiary_cutoff)
    questions_id = []
    questions = {}
    for r in retriever:
        score = r[1]
        questions_id.append(r[0].metadata["unique_id"])
        questions[r[0].metadata["unique_id"]] = [r[0].page_content, score, r[0].metadata["threshold"]]

    cypher = f"""
        MATCH (target)-[r: ANSWER]->(source)
        WHERE target.type = 'Answers' AND source.unique_id IN {questions_id}
        RETURN DISTINCT target, source.unique_id AS question_id
    """
    result = kg.query(cypher)

    results = {}
    for r in result:
        results[questions[r["question_id"]][0]] = [r["target"]["text"], questions[r["question_id"]][1],
                                                   questions[r["question_id"]][2]]

    sorted_data = dict(sorted(results.items(), key=lambda item: item[1][1], reverse=True))
    return sorted_data


async def query(neo4j_vector_store, query, similiary_top_k, similiary_cutoff, metadata):
    query_type = metadata["query_type"] if "query_type" in metadata.keys() else "CHUNK"
    if query_type == "QA":
        return await query_QA(neo4j_vector_store, query, similiary_top_k, similiary_cutoff, metadata)
    elif query_type == "CHUNK":
        return await aquery(neo4j_vector_store, query, similiary_top_k, similiary_cutoff, metadata)
    else:
        return await aquery(neo4j_vector_store, query, similiary_top_k, similiary_cutoff, metadata)


async def get_doc(business, metadata, start_index, limit):
    metadata["business"] = business
    metadata_str = ",".join(["{key}: '{value}'".format(key=key, value=value) for key, value in metadata.items()])
    check_query = f"""
        MATCH (n {{{metadata_str}}})
        RETURN n
        SKIP {start_index}
        LIMIT {limit}
        """
    result = kg.query(check_query)
    return [r[0].page_content for r in result]


async def delete_index(business):
    cypher = f"""
        MATCH (n {{business: '{business}'}})
        DETACH DELETE n
        """
    kg.query(cypher)
    logger.info("All nodes and relationships have been deleted from the database.")


def delete_relationship(node1_label, node1_unique_id, from_key, node2_label, node2_unique_id, to_key):
    if node1_label is None and node1_unique_id is None and from_key is None:
        check_query = f"""
            MATCH (node)-[r]->(n2:{node2_label} {{{to_key}: "{node2_unique_id}"}})
            DELETE r RETURN node
            """
    elif node2_label is None and node2_unique_id is None and to_key is None:
        check_query = f"""
            MATCH (n1:{node1_label} {{{from_key}: "{node1_unique_id}"}})-[r]->(node)
            DELETE r RETURN node
            """
    else:
        check_query = f"""
            MATCH (from:{node1_label} {{{from_key}: "{node1_unique_id}"}})-[r]->(to:{node2_label} {{{to_key}: "{node2_unique_id}"}})
            DELETE r RETURN from, to
            """
    result = kg.query(check_query)

    if len(result) > 0 and "node" in result[0].keys():
        # cypher_answers = f"""
        #         MATCH (a {{unique_id: '{result[0]["node"]["unique_id"]}'}})-[r:ANSWER]->()
        #         WITH a, COUNT(r) AS rel_count
        #         WHERE rel_count = 0
        #         DETACH DELETE a
        #         """
        # answer = kg.query(cypher_answers)
        cypher_get_rel_count = f"""
        MATCH (a {{unique_id: "{result[0]["node"]["unique_id"]}"}})-[r:ANSWER]->()
        WITH a, COUNT(r) AS rel_count
        RETURN a.unique_id AS id, rel_count
        """

        # 执行查询并获取结果
        result_rel_count = kg.query(cypher_get_rel_count)

        if len(result_rel_count) == 0:
            # # 输出 rel_count 以供调试或记录
            # unique_id = result_rel_count[0]['id']
            # rel_count = result_rel_count[0]['rel_count']
            # print(f"Node ID: {unique_id}, Relation Count: {rel_count}")
            #
            # # 判断是否执行删除操作（如果确实需要删除）
            # if rel_count == 0:

            cypher_delete = f"""
            MATCH (a {{unique_id: "{result[0]["node"]["unique_id"]}"}})
            DETACH DELETE a
            """
            kg.query(cypher_delete)

    if result:
        logger.info(f"Relationship between {node1_label} and {node2_label} has been deleted!")


def delete_index_QA(business, q_unique_id):
    cypher = f"""
        MATCH (n {{unique_id: '{q_unique_id}', business: '{business}'}})
        OPTIONAL MATCH (n)<-[r]-(a)
        RETURN a.unique_id AS unique_id
        """
    a_unique_id = kg.query(cypher)[0]["unique_id"]

    delete_cypher = f"""
        MATCH (n {{unique_id: '{q_unique_id}', business: '{business}'}})
        DETACH DELETE n
        """
    kg.query(delete_cypher)

    cypher_answers = f"""
        MATCH (a {{unique_id: '{a_unique_id}', business: '{business}'}})-[r:ANSWER]->()
        WITH a, COUNT(r) AS rel_count
        WHERE rel_count = 0
        DETACH DELETE a
        """
    answer = kg.query(cypher_answers)

    logger.info(f"Node with unique_id {q_unique_id} and its related 'Answers' nodes have been deleted from the {answer}"
                f" label if they had no other relationships.")


def delete_all():
    cypher = """
    MATCH (n)
    DETACH DELETE n
    """
    kg.query(cypher)
    logger.info("All nodes and relationships have been deleted from the database.")

    # Optionally, you can also delete indexes if needed
    index_cypher = """
    show index
    """
    indexes = kg.query(index_cypher)
    logger.info(indexes)
    for index in indexes:
        if index["owningConstraint"] is not None:
            drop_cypher = f"DROP CONSTRAINT {index['name']}"
        else:
            drop_cypher = f"DROP INDEX {index['name']}"
        logger.info(f"Executing: {drop_cypher}")
        kg.query(drop_cypher)
    logger.info("All indexes have been deleted from the database.")


if __name__ == '__main__':
    import asyncio

    # documents = file_loader(root_path + 'uploads/QA.txt')
    # create_documents(documents)
    # logger.info(asyncio.run(query("大概要多久才能回复")))
    # new = switch_index("test", "test")

    asyncio.run(
        acreate_QA(Q={"text": "需要停吗？", "unique_id": "93D503DCB466930ABE8B93F82B5DB960_0Questions", "file_name": "test", "file_id": "text"},
                   A={"text": "保健品一般都是可以长期吃的。",
                      "unique_id": "93D503DCB466930ABE8B93F82B5DB960_01Answers", "file_name": "test", "file_id": "text"},
                   business="FAQ"))

    # logger.info(asyncio.run(
    #     aquery(switch_index("FAQ"), "谷类主食300到350克，油25克，牛奶半斤到一斤", 3, 0.3, {"business": "中老年养生问题",
    #                                                                       "type": "Questions"})
    # ))
    # delete_all()
    # delete_index_QA("", "93D503DCB466930ABE8B93F82B5DB960_8Answers")
