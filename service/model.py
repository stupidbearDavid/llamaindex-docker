import os
import time
import torch

from transformers import AutoTokenizer, AutoModelForSequenceClassification

import numpy as np
from typing import List
from llama_index import LangchainEmbedding, ServiceContext
from langchain.embeddings.huggingface import HuggingFaceEmbeddings
from llama_index.vector_stores import RedisVectorStore
from config import redis_url, redis_pwd, redis_port, email, chunk_size, overlap_size, root_path, cuda_num, db_type
from copy import deepcopy
from service.logger import logger

url = "redis://:{}@{}:{}".format(redis_pwd, redis_url, redis_port)


class SingletonMeta(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(SingletonMeta, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class IndexStore:

    def __init__(self, num_gpu):

        # embedding_model_name = "Multilingual-E5-base"
        self.embedding_model_name = "bce-embedding-base"

        self.email_account = email.strip().split(',')

        self.chunk_prefix = ""
        self.chunk_query_prefix = ""
        if self.embedding_model_name == "Multilingual-E5-base":
            self.chunk_prefix = "passage: "
            self.chunk_query_prefix = "query: "

        self.max_length = 512
        self.overlap_tokens = 80
        self.tokenizer = AutoTokenizer.from_pretrained(root_path + "service/bce-reranker-base")
        self.sep_id = self.tokenizer.sep_token_id
        self.num_gpu = num_gpu

        self.instance = self._create_instants(self.num_gpu)

    def _create_instants(self, gpu_id):
        device = "cpu" if not torch.cuda.is_available() else "cuda:{}".format(gpu_id)
        embedding_model = HuggingFaceEmbeddings(
            model_name=root_path + "service/" + self.embedding_model_name, model_kwargs={'device': device})

        embed_model = LangchainEmbedding(embedding_model, self.chunk_prefix, self.chunk_query_prefix)
        reranker_model = AutoModelForSequenceClassification.from_pretrained(
            root_path + "service/bce-reranker-base")
        reranker_model.to(device)

        service_context = ServiceContext.from_defaults(embed_model=embed_model, chunk_size=chunk_size,
                                                       chunk_overlap=overlap_size)

        return {"embedding_model": embedding_model, "reranker_model": reranker_model,
                "service_context": service_context}

    def _merge_inputs(self, chunk1_raw, chunk2):
        chunk1 = deepcopy(chunk1_raw)
        chunk1['input_ids'].extend(chunk2['input_ids'])
        chunk1['input_ids'].append(self.sep_id)
        chunk1['attention_mask'].extend(chunk2['attention_mask'])
        chunk1['attention_mask'].append(chunk2['attention_mask'][0])
        if 'token_type_ids' in chunk1:
            token_type_ids = [1 for _ in range(len(chunk2['token_type_ids']) + 1)]
            chunk1['token_type_ids'].extend(token_type_ids)
        return chunk1

    def tokenize_preproc(
            self,
            query: str,
            passages: List[str]
    ):
        query_inputs = self.tokenizer.encode_plus(query, truncation=False, padding=False)
        max_passage_inputs_length = self.max_length - len(query_inputs['input_ids']) - 1
        assert max_passage_inputs_length > 100, "Your query is too long! Please make sure your query less than 400 tokens!"
        overlap_tokens = min(self.overlap_tokens, max_passage_inputs_length // 4)

        res_merge_inputs = []
        res_merge_inputs_pids = []
        for pid, passage in enumerate(passages):
            passage_inputs = self.tokenizer.encode_plus(passage, truncation=False, padding=False,
                                                        add_special_tokens=False)
            passage_inputs_length = len(passage_inputs['input_ids'])

            if passage_inputs_length <= max_passage_inputs_length:
                qp_merge_inputs = self._merge_inputs(query_inputs, passage_inputs)
                res_merge_inputs.append(qp_merge_inputs)
                res_merge_inputs_pids.append(pid)
            else:
                start_id = 0
                while start_id < passage_inputs_length:
                    end_id = start_id + max_passage_inputs_length
                    sub_passage_inputs = {k: v[start_id:end_id] for k, v in passage_inputs.items()}
                    start_id = end_id - overlap_tokens if end_id < passage_inputs_length else end_id

                    qp_merge_inputs = self._merge_inputs(query_inputs, sub_passage_inputs)
                    res_merge_inputs.append(qp_merge_inputs)
                    res_merge_inputs_pids.append(pid)

        return res_merge_inputs, res_merge_inputs_pids

    def rerank(
            self,
            query: str,
            passages: List[str],
            batch_size: int = 256,
            **kwargs
    ):
        gpu_id = self.num_gpu
        device = "cpu" if not torch.cuda.is_available() else "cuda:{}".format(gpu_id)
        # remove invalid passages
        passages = [p for p in passages if isinstance(p, str) and 0 < len(p) < 16000]
        if query is None or len(query) == 0 or len(passages) == 0:
            return {'rerank_passages': [], 'rerank_scores': []}

        # preproc of tokenization
        sentence_pairs, sentence_pairs_pids = self.tokenize_preproc(query, passages)

        tot_scores = []
        with torch.no_grad():
            for k in range(0, len(sentence_pairs), batch_size):
                batch = self.tokenizer.pad(
                    sentence_pairs[k:k + batch_size],
                    padding=True,
                    max_length=None,
                    pad_to_multiple_of=None,
                    return_tensors="pt"
                )
                batch_on_device = {k: v.to(device) for k, v in batch.items()}
                scores = self.instance["reranker_model"](**batch_on_device, return_dict=True).logits.view(
                    -1, ).float()
                scores = torch.sigmoid(scores)
                tot_scores.extend(scores.cpu().numpy().tolist())

        # ranking
        merge_scores = [0 for _ in range(len(passages))]
        for pid, score in zip(sentence_pairs_pids, tot_scores):
            merge_scores[pid] = max(merge_scores[pid], score)

        merge_scores_argsort = np.argsort(merge_scores)[::-1]
        sorted_passages = []
        sorted_scores = []
        for mid in merge_scores_argsort:
            sorted_scores.append(merge_scores[mid])
            sorted_passages.append(passages[mid])

        return {
            'rerank_passages': sorted_passages,
            'rerank_scores': sorted_scores
        }


class Singleton(metaclass=SingletonMeta):
    _instances = []
    if db_type == "redis":
        indexlist_vector_store = RedisVectorStore(
            index_name="index_list",
            index_prefix="index_list",
            redis_url=url,
            overwrite=False
        )
        apilist_vector_store = RedisVectorStore(
            index_name="apilist",
            index_prefix="apilist'",
            redis_url=url,
            overwrite=False
        )

    @classmethod
    def initialize_instances(cls, num_instances):
        cls._instances = [IndexStore(i) for i in range(num_instances)]

    @classmethod
    def instance(cls):
        gpu_id = 0 if cuda_num == 1 else int(time.time() * 1000) % cuda_num
        logger.info("switch to gpu: {}".format(gpu_id))
        return cls._instances[gpu_id]


Singleton.initialize_instances(cuda_num)

if __name__ == '__main__':
    query = "胡子哥，你啥时候开始直播"
    passages = ["保健品服用方法，或者康养知识", "与保健品服用方法，或者康养知识不相关"]
    print(Singleton.instance().rerank(query, passages))
    # print(Singleton.indexlist_vector_store)
