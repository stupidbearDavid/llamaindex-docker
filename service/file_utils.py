import os
import json


def save_file(data, filename):
    with open(filename, "w", encoding="utf-8") as output:
        output.write(data)


def load_file(filename):
    old_file_data = json.load(open(filename, encoding="utf-8"))
    return old_file_data



