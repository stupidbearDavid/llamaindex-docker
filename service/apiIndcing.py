import os
import json
import time

import traceback
from service.logger import logger

from llama_index import VectorStoreIndex, Document
from llama_index.vector_stores import RedisVectorStore
from service.model import url, Singleton

from service.indcing import delete_doc
from service.redis_index import get_md5_hash
from service.authority import get_authoritylist
from config import db_type

vector_store = None
if db_type == "redis":
    vector_store = RedisVectorStore(
        index_name="apilist",
        index_prefix="apilist",
        redis_url=url,
        overwrite=False
    )


def switch_business(business):
    vector_store = RedisVectorStore(
        index_name=business,
        index_prefix=business,
        redis_url=url,
        overwrite=False
    )
    INDEX = VectorStoreIndex.from_vector_store(vector_store=vector_store,
                                               service_context=Singleton.instance().instance["service_context"])
    if not Singleton.apilist_vector_store.client.exists("apilist"):
        Singleton.apilist_vector_store.client.append("apilist", "{\"" + business + "\": {}}")
    logger.info("switch index to: {}".format(business))
    return INDEX


def get_apilist():
    if Singleton.apilist_vector_store.client.exists("apilist"):
        data = Singleton.apilist_vector_store.client.get("apilist")
        index_list = json.loads(data)
    else:
        Singleton.apilist_vector_store.client.append("apilist", "{\"" + "api_service" + "\": {}}")
        index_list = {"\"" + "api_service" + "\"": {}}
    return index_list


def add_api_index(chunk, business):
    INDEX = switch_business(business)
    if len(chunk["description"]) > 100:
        return {
            'status': 400,
            'message': "Length of description must be smaller than 100 vs {}".format(len(chunk["description"]))
        }
    start = 0
    filename = get_md5_hash(str(time.time()))
    document = Document(text=chunk["description"], file_name=filename,
                        metadata={"file_name": filename, "Api_meta": json.dumps({
                            "name": chunk["name"], "params": chunk["params"], "method": chunk["method"],
                            "headers": chunk["headers"],
                            "api_url": chunk["api_url"]})}, start_char_idx=start)
    if not document.text == "":
        INDEX.insert(document)
    else:
        return {
            'status': 400,
            'message': "Empty contents!"
        }
    return {
        'status': 200,
        'message': document.get_doc_id()
    }


def new_business(business):
    index_list = get_apilist()
    if business in index_list.keys():
        logger.info("business: {} already exists!".format(business))
        return
    index_list[business] = {}
    authortiy_list = get_authoritylist()
    if business in authortiy_list.keys():
        logger.info("business: {} already exists!".format(business))
        return
    authortiy_list[business] = {}
    vector_store.client.set("authority_list", json.dumps(authortiy_list))
    Singleton.apilist_vector_store.client.set("apilist", json.dumps(index_list))
    logger.info("switch business is started...")


def delete_business(business: str):
    index_list = get_apilist()
    if business == "apilist":
        return
    if not business in index_list.keys():
        return
    filelist = index_list.pop(business)
    for filename in filelist:
        delete_doc(business, filename)
    INDEX = switch_business(business)
    try:
        INDEX.vector_store.delete_index()
    except Exception:
        logger.info(traceback.format_exc())
    Singleton.apilist_vector_store.client.set("apilist", json.dumps(index_list))
    authortiy_list = get_authoritylist()
    authortiy_list.pop(business)
    vector_store.client.set("authority_list", json.dumps(authortiy_list))
    logger.info("Finished delete business: \'" + business + "\'!")
