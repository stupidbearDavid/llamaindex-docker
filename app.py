#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
import argparse
import os

from aiohttp import web
import aiohttp_cors

from service.webservice.llamaindex.deleteHander import DeleteRequest
from service.webservice.llamaindex.deleteIndexHander import DeleteIndexRequest
from service.webservice.llamaindex.authorityEditHander import AuthorityEditRequest
from service.webservice.llamaindex.fileHander import FileRequest
from service.webservice.llamaindex.questionAnswerHander import QuestionAnswerRequest
from service.webservice.llamaindex.indexListHander import IndexListRequest
from service.webservice.llamaindex.newIndexHander import NewIndexRequest
from service.webservice.llamaindex.viewHander import ViewRequest
from service.webservice.llamaindex.contentHander import ContentRequest
from service.webservice.llamaindex.authorityHander import AuthorityRequest
from service.webservice.llamaindex.deleteAuthorityHander import DeleteAuthRequest
from service.webservice.llamaindex.newAuthorityHander import NewAuthRequest
from service.webservice.llamaindex.isOwnerHander import IsOwnerRequest
from service.webservice.llamaindex.queryHander import QueryRequest
from service.webservice.llamaindex.updateDataHander import PushDataRequest
from service.webservice.llamaindex.clearNeo4jHander import ClearNeo4jIndexRequest
from service.webservice.llamaindex.deleteDataHander import DeleteDataRequest
from service.webservice.llamaindex.relationHander import RelationRequest
from service.webservice.llamaindex.characterRelationHander import AddCharacterRelationRequest
from service.webservice.llamaindex.healthCheckHander import HealthCheckRequest
from service.webservice.llamaindex.updateItemDataHander import PushItemDataRequest
from service.webservice.llamaindex.queryItemHander import QueryItemRequest
from service.webservice.llamaindex.queryItemNoFilterHander import QueryItemRequest as QueryItemRequestNoFilter
from service.webservice.llamaindex.queryChunkHander import QueryChunkRequest
from service.webservice.llamaindex.updateKnowledgeHander import PushKnowledgeDataRequest
from service.webservice.llamaindex.queryKnowledgeHander import QueryKnowledgeRequest
from service.webservice.llamaindex.queryLiveChunkHander import QueryChunkLiveRequest
from service.webservice.llamaindex.queryRelationHander import QueryRelationRequest
from service.webservice.llamaindex.querySimilarHander import QuerySimilarRequest
from service.webservice.llamaindex.queryRelatedHander import QueryRelatedRequest
from service.webservice.llamaindex.updateRelationHander import PushRelationDataRequest
from service.webservice.llamaindex.updateLiveHander import PushLiveFileRequest
from service.webservice.llamaindex.itemContentHander import ItemContentRequest

from service.webservice.api_service.queryApiHander import QueryApiRequest
from service.webservice.api_service.addApiHander import AddIndexRequest
from service.webservice.api_service.newBusinessHander import NewBusinessRequest
from service.webservice.api_service.deleteBusinessHander import DeleteBusinessRequest

from service.webservice.business_api.infoReqHander import InfoReqRequest
from service.webservice.business_api.schoolRecHander import SchoolRecRequest


async def init_app():
    app = web.Application()
    # Configure default CORS settings
    cors = aiohttp_cors.setup(app, defaults={
        "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*",
        )
    })

    # 添加多个路由，并启用 CORS
    route_configs = [
        # knowledge upload books

        (r'/delete', DeleteRequest, ['POST']),
        (r'/view', ViewRequest, ['POST']),
        (r'/content', ContentRequest, ['POST']),
        (r'/authority_edit', AuthorityEditRequest, ['POST']),
        (r'/new_index', NewIndexRequest, ['POST']),
        (r'/apilist', IndexListRequest, ['POST']),
        (r'/delete_index', DeleteIndexRequest, ['POST']),
        (r'/authority', AuthorityRequest, ['POST']),
        (r'/delete_auth', DeleteAuthRequest, ['POST']),
        (r'/new_auth', NewAuthRequest, ['POST']),
        (r'/is_owner', IsOwnerRequest, ['POST']),
        (r'/get_query', IsOwnerRequest, ['POST']),
        (r'/add_qa_content', QuestionAnswerRequest, ['POST']),
        # (r'/update', PushDataRequest, ['POST']),

        # FAQ query
        (r'/query', QueryRequest, ['POST']),
        # FAQ update
        (r'/query/update', PushDataRequest, ['POST']),

        # Items query
        (r'/items/query', QueryItemRequest, ['POST']),
        (r'/items_no_filter/query', QueryItemRequestNoFilter, ['POST']),
        # Items update
        (r'/items/query/update', PushItemDataRequest, ['POST']),

        # knowledge query
        (r'/docs/query', QueryChunkRequest, ['POST']),
        # knowledge update
        (r'/docs/query/update', PushKnowledgeDataRequest, ['POST']),
        # knowledge upload file
        (r'/docs/query/upload', FileRequest, ['POST']),

        # relation query
        (r'/relation/query', QueryRelationRequest, ['POST']),
        # knowledge update
        (r'/relation/query/update', PushRelationDataRequest, ['POST']),

        # live query
        (r'/live/query', QueryChunkLiveRequest, ['POST']),
        # live update
        (r'/live/query/update', PushLiveFileRequest, ['POST']),

        # combined knowledge query
        (r'/knowledge/query', QueryKnowledgeRequest, ['POST']),

        # is related query
        (r'/is_related/query', QueryRelatedRequest, ['POST']),

        # is similar query
        (r'/is_similar/query', QuerySimilarRequest, ['POST']),

        (r'/item_content', ItemContentRequest, ['get']),

        (r'/delete_qa', DeleteDataRequest, ['POST']),
        (r'/add_relation', RelationRequest, ['POST']),
        (r'/add_character_relation', AddCharacterRelationRequest, ['POST']),
        (r'/clear_neo4j', ClearNeo4jIndexRequest, ['GET']),
        (r'/health', HealthCheckRequest, ['GET']),

        (r'/query_api', QueryApiRequest, ['POST']),
        (r'/add_api_index', AddIndexRequest, ['POST']),
        (r'/add_business', NewBusinessRequest, ['POST']),
        (r'/delete_business', DeleteBusinessRequest, ['POST']),

        (r'/gaokao/get_info', InfoReqRequest, ['POST']),
        (r'/gaokao/recommendation', SchoolRecRequest, ['POST']),
    ]

    for path, handler, methods in route_configs:
        resource = cors.add(app.router.add_resource(path))
        for method in methods:
            cors.add(resource.add_route(method, handler))

    return app

if __name__ == "__main__":
    root_path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + os.path.sep + ".") + '/'

    app = init_app()

    web.run_app(app, port=8888)
